<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Session;
use Redirect;
use Illuminate\Notifications\Messages\MailMessage;
use Validator;
use Twilio;
use Hash;
use URL;
use Log;
use Route;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->redirectTo = URL::previous();
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('user.auth.login');
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */

    public function username()
    {       
        return 'phone';  
    }   

    public function login(Request $request){

        Log::info($request->all());
        $this->validate($request, [
            'mobile' => 'required',
            'password' => 'required',

            ]);
        if (\Auth::attempt([
            'phone' => $request->country_code . $request->mobile,
            'password' => $request->password])
        ){
            return redirect('/');
        }
        return redirect('/')->with('flash_error', 'Invalid Credential!!');
    }

    public function api_login(Request $request)
    {

        $tokenRequest = $request->create('/oauth/token', 'POST', $request->all());
        $request->request->add([
           "client_id"     => $request->client_id,
           "client_secret" => $request->client_secret,
           "grant_type"    => 'password',
           "code"          => '*',
        ]);
        $response = Route::dispatch($tokenRequest);

        $json = (array) json_decode($response->getContent());
        //      $json['status'] = true;

        $response->setContent(json_encode($json));

        
        $update = User::where('phone', $request->username)->update(['device_token' => $request->device_token , 'device_id' => $request->device_id , 'device_type' => $request->device_type]);    
        
        return $response;
    }


     public function forgot_password_mail(Request $request) {
\Log::info($request->all());
        /*$this->validate($request, [
            'email' => 'required|exists:users,email',
        ]);*/
 //return $request->all();
        try {
            $user = User::where('email' , $request->email)->first();
            if(count($user) == 0){
                 return response()->json([
                    'message' => 'User Not Exist',
                ]);
            }

            $otp = rand(100000, 999999);
            $user->otp = $otp;
            $user->save();

            site_sendmail($user,'forgot_password','Forgot Password','user');

                return response()->json([
                    'message' => 'OTP Sent!',
                    'user' => $user
                ]);
           
           // return response()->json(['error' => $msg_data], 422);
        } catch(Exception $e) {
            return response()->json([
                'error' => trans('form.whoops')
            ], 500);
        }
    }

     public function checkotpmail(Request $request) {
\Log::info($request->all());
        try {
            $user = User::where('email' , $request->email)->where('otp' , $request->otp)->first();
            if(count($user) == 0){
                 return response()->json([
                    'message' => 'Otp wrong',
                ]);
            }

            return response()->json([
                'message' => 'Success'
            ]);
           
        } catch(Exception $e) {
            return response()->json([
                'error' => trans('form.whoops')
            ], 500);
        }
    }

    public function reset_password(Request $request){
\Log::info($request->all());
        $this->validate($request, [
            'password' => 'required|confirmed|min:6',
            'id' => 'required|numeric|exists:users,id'
        ]);

        try{

            $User = User::findOrFail($request->id);
            $User->password = bcrypt($request->password);
            $User->save();

            if($request->ajax()) {
                return response()->json(['message' => 'Password Updated']);
            }else{
               return redirect('/')->with('flash_success', 'Password Changed Successfully.'); 
            }
        }catch (Exception $e) {
            if($request->ajax()) {
                return response()->json(['error' => trans('form.whoops')], 500);
            }
        }
    }
    
}
