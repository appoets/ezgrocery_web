<?php

namespace App\Http\Controllers\Resource;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Favorite;
class FavoriteResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $available = Favorite::avalability('enabled')->where('favorites.user_id',$request->user()->id)->get();
       // dd($available);
        $un_available = Favorite::avalability('disabled')->where('favorites.user_id',$request->user()->id)->get();
        if($request->ajax()){
            return [
            'available' => $available,
            'un_available' => $un_available
            ];
        }else{
            return view('user.shop.favourites',compact('available'));
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \Log::info($request->all());
        \Log::info('add');
        $this->validate($request, [
            'shop_id' => 'required'
        ]);
        try {
            $Favorite_del = Favorite::where('product_id',$request->product_id)->where('user_id',$request->user()->id)->get();
            if(count($Favorite_del) != 0){
                Favorite::where('product_id',$request->product_id)->where('user_id',$request->user()->id)->delete();
                return response()->json(['message' => trans('form.favorite.un_favorite')]);
            }
            $Favorite = Favorite::withTrashed()->where('user_id',$request->user()->id)->where('product_id',$request->product_id)->first();
            if(!$Favorite){
                $Favorite = Favorite::create([
                    'shop_id' => $request->shop_id,  
                    'user_id' => $request->user()->id,
                    'product_id' => $request->product_id

                ]);
            }else{
                $Favorite->restore();
            }

           return response()->json(['message' => trans('form.favorite.favorite')]);
        }catch (ModelNotFoundException $e) {
            \Log::info($e);
             return response()->json(['error' => trans('form.whoops')]);
        }catch(Exception $e){
            \Log::info($e);
             return response()->json(['error' => trans('form.whoops')]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        
        try {
            $Favorite = Favorite::where('user_id',$request->user()->id)->where('shop_id',$id)->first();
           
            return $Favorite?:[];
        }catch (ModelNotFoundException $e) {
             return response()->json(['error' => trans('form.whoops')]);
        }catch(Exception $e){
             return response()->json(['error' => trans('form.whoops')]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        \Log::info($request->all());
         \Log::info('up');
        try{
            $Favorite = Favorite::where('product_id',$id)->where('user_id',$request->user()->id)->firstOrFail();
            $Favorite->delete();
            return response()->json(['message' => trans('form.favorite.un_favorite')]);
        }catch (ModelNotFoundException $e) {
             return response()->json(['message' => trans('form.no_data_found')]);
        }catch(Exception $e){
             return response()->json(['error' => trans('form.whoops')]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {  
    \Log::info($request->all());
     \Log::info($id);
         \Log::info('de'); 
        try{
            $Favorite = Favorite::where('product_id',$id)->where('user_id',$request->user()->id)->firstOrFail();
            $Favorite->delete();
            return response()->json(['message' => trans('form.favorite.un_favorite')]);;
        }catch (ModelNotFoundException $e) {
             return response()->json(['message' => trans('form.no_data_found')]);
        }catch(Exception $e){
             return response()->json(['error' => trans('form.whoops')]);
        }
    }
}
