<?php

namespace App\Http\Controllers\UserResource;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\UserRequests;
use App\Helpers\Helper;
use App\Http\Controllers\ProviderResources\TripController;
use Auth;
use Setting;
use App\User;
use App\Provider;
use App\ReferralHistroy;
use App\ReferralEarnings;
use App\UserWallet;
use DB;
use Validator;

class ReferralResource extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Setting::get('referral') == 0){
            return redirect('/');
        }
        
        // $referrals  =$this->get_referral(1,Auth::user()->id); 
        $referrals  = $ReferralHistroy=ReferralEarnings::where('referrer_id',Auth::user()->id)->get( array(
            DB::raw( 'COALESCE(SUM(count), 0) AS total_count' ),
            DB::raw( 'COALESCE(SUM(amount), 0) AS total_amount' ),
        ));
      //  dd($referrals);
        return view('user.referral',compact('referrals'));
        // return view('user.referral');
    }

    

    public function checkReferralCode(array $data)
    {
        $rules = [
            'referral_unique_id'  => 'unique:users',
        ];

        $messages = [
            'referral_unique_id.unique'  => 'referral_code_already exits',
        ];

        return Validator::make($data,$rules,$messages);
    }

    public function generateCode($length = 6) {
       
        $az = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $azr = rand(0, 51);
        $azs = substr($az, $azr, 10);
        $stamp = hash('sha256', time());
        $mt = hash('sha256', mt_rand(5, 20));
        $alpha = hash('sha256', $azs);
        $hash = str_shuffle($stamp . $mt . $alpha);
        $code = strtoupper(substr($hash, $azr, $length));        
        $data['referral_unique_id']=$code;

        $validator  = $this->checkReferralCode($data);

        if ($validator->fails()) {            
            $this->generateCode();
        }    
        
        return $code;

    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function create_referral($referral_code,$referral_data)
    {
        
        $referral=User::where('referral_unique_id',$referral_code)->first();

        if(!empty($referral)){

            //insert referral histroy
            $User = ReferralHistroy::create([
                'referrer_id' => $referral->id,
                'referral_id' => $referral_data->id,
                'referral_data' => json_encode($referral_data),
                'status' => 'P'            
            ]);

            if(Setting::get('referral_amount')>0){
            
                $ReferralHistroy=ReferralHistroy::select(DB::raw("group_concat(id) as ids"))->where('referrer_id',$referral->id)->where('status','P')->groupBy('referrer_id')->get();

                $Referralcount=0;$ids=NULL;

                if($ReferralHistroy->count()>0){
                    $Referralcount=count(explode(',',$ReferralHistroy[0]->ids));
                    $ids=$ReferralHistroy[0]->ids;
                }
                if(Setting::get('referral_amount')>0){

                
                if(Setting::get('referral_count')>0 && Setting::get('referral_count') == $Referralcount) {                
                    //create referral earnings
                    $Earnings = ReferralEarnings::create([
                        'referrer_id' => $referral->id,
                        'amount' => Setting::get('referral_amount', 0),
                        'count' => $Referralcount,
                        'referral_histroy_id' => $ids            
                    ]);
                    //create amount to user wallet
                    $this->referralCreditDebit(Setting::get('referral_amount'),$referral->id);

                    //update histroy table process status to complete
                    ReferralHistroy::where('referrer_id',$referral->id)->where('status','P')->update(['status' => 'C']);
                                  
                }
            }
        }            
    }
}

    public function get_referral($user_id)
    {
        $ReferralHistroy=ReferralEarnings::where('referrer_id',$user_id)->get( array(
            DB::raw( 'COALESCE(SUM(count), 0) AS total_count' ),
            DB::raw( 'COALESCE(SUM(amount), 0) AS total_amount' ),
        ));
        
        return $ReferralHistroy;                   
    }


    //function
    public function referralCreditDebit($amount,$UserRequest){

			$msg=trans('api.transaction.referal_recharge');           
			$ttype='C';
			$user_data=UserWallet::orderBy('id', 'DESC')->first();
			if(!empty($user_data))
				$transaction_id=$user_data->id+1;
			else
				$transaction_id=1;

			$transaction_alias= $this->transationAlias('user', 'refer');

			$user_id=$UserRequest;
			$transaction_type=12;

			$ipdata=array();
			$ipdata['transaction_id']=$transaction_id;
			$ipdata['transaction_alias']=$transaction_alias;
			$ipdata['transaction_desc']=$msg;
			$ipdata['id']=$user_id;        
			$ipdata['type']=$ttype;
			$ipdata['amount']=$amount;
			$this->createUserWallet($ipdata);
 

		return true;
    }
    
    protected function createUserWallet($request){
		
		$user=User::findOrFail($request['id']);

		$userWallet=new UserWallet;
		$userWallet->user_id=$request['id']; 
		$userWallet->transaction_id=$request['transaction_id'];        
		$userWallet->transaction_alias=$request['transaction_alias'];
		$userWallet->transaction_desc=$request['transaction_desc'];
		$userWallet->type=$request['type'];
		$userWallet->amount=$request['amount'];        

		if(empty($user->wallet_balance))
			$userWallet->open_balance=0;
		else
			$userWallet->open_balance=$user->wallet_balance;

		if(empty($user->wallet_balance))
			$userWallet->close_balance=$request['amount'];
		else            
			$userWallet->close_balance=$user->wallet_balance+($request['amount']);

		$userWallet->save();

		//update the user wallet amount to user table        
		$user->wallet_balance=$user->wallet_balance+($request['amount']);
		$user->save();

		return $userWallet;
    }
    public function transationAlias($userType, $paymentType = null) {
		if($userType == 'user') {
			$user_data=UserWallet::orderBy('id', 'DESC')->first();
			$prefix = ($paymentType != null) ? 'RFU' : 'URC';
		}
		
		if(!empty($user_data))
		$transaction_id=$user_data->id+1;
		else
		   $transaction_id=1;

		return $prefix.str_pad($transaction_id, 6, 0, STR_PAD_LEFT);
	}

}
