<?php

namespace App\Http\Controllers\UserResource;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\UserCart;
use App\Promocode;
use App\Addon;
use App\CartAddon;

use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Zone;
use App\UserAddress;
use App\PromocodeUsage;
use App\WalletPassbook;
use Carbon\Carbon;

use Auth;
use Log;
use Setting;
use Exception;
use Session;
use App\Order;
use App\Product;
use App\Shop;
use App\ProductPrice;

class UpdateCartResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try {
           
            $Item = UserCart::findOrFail($id);
            $Item->delete();
            CartAddon::where('user_cart_id',$id)->delete();
            if($request->ajax()){
                return response()->json(['message' => 'Product was removed from your cart.']);
            }
            return back()->with('flash_error', 'Delete cart Item Successfully!');
        } catch (ModelNotFoundException $e) {
            return response()->json(['message' => trans('form.whoops')], 500);
        }
    }

    public function apply_promocode(Request $request)
    {
        $CartItems = UserCart::list($request->user()->id);
        $tot_price = 0;$discount = 0;$tax  =0; $promocode_amount = 0; $total_net = 0; $total_wallet_balance = 0;$payable = 0;
        if(!$CartItems->isEmpty()) {
            try {
                // Shop finding logic goes here.
                $Shop_id = Product::select('shop_id')->where('id',$CartItems[0]->product_id)->first()->shop_id;
                $Shop = Shop::findOrFail($Shop_id); 
                foreach($CartItems as $Product){
                    $tot_qty = $Product->quantity;
                    $tot_price += $Product->quantity * ProductPrice::where('product_id',$Product->product_id)->first()->orignal_price;
                    $tot_price_addons = 0;
                    if(count($Product->cart_addons)>0){
                        foreach($Product->cart_addons as $Cartaddon){

                           $tot_price_addons += $Cartaddon->quantity * $Cartaddon->addon_product->price; 
                        }
                    }
                    $tot_price += $tot_qty*$tot_price_addons; 
                    
                }
                $tot_price = $tot_price;
                $net = $tot_price;
                if($Shop->offer_percent){
                    if($tot_price > $Shop->offer_min_amount){
                       //$discount = roundPrice(($tot_price*($Order->shop->offer_percent/100)));
                       $discount = ($tot_price*($Shop->offer_percent/100));
                       //if()
                       $net = $tot_price - $discount;
                    }
                }
                $total_wallet_balance = 0;
                $tax = ($net*Setting::get('tax')/100);
                // $tips= $request->tips;
                // $total_net = ($net+$tax+$tips);
                $total_net = $net;


                if($request->has('promocode_code') && !empty($request->promocode_code)) { 
                        $find_promo = Promocode::where('promo_code',$request->promocode_code)->first();
                        $promocode_id = $find_promo->id;
                        $promocode_product_id = $find_promo->product_id;

                        // Session::put(['promocode_id' => $find_promo->id]);

                        if($find_promo->promocode_type=='amount'){
                            $promocode_amount = $find_promo->discount;
                        }else{
                            $promocode_amount = ($total_net*($find_promo->discount/100));
                        }
                        foreach($CartItems as $Product){
                        
                        $CartProduct = UserCart::where('product_id', $promocode_product_id)->where('user_id', Auth::user()->id)->first();
                        // dd($CartProduct);

                        if($promocode_product_id == $Product->product_id){

                            $price_without_promo = $Product->quantity * ProductPrice::where('product_id',$Product->product_id)->first()->orignal_price;

                            if($price_without_promo >= $promocode_amount){
                                $CartProduct->user_id = Auth::user()->id;
                                // $CartProduct->product_id = $Product->product_id;
                                // $CartProduct->quantity = $Product->quantity;
                                 $CartProduct->promocode_id = $promocode_id;
                                 $CartProduct->save();
    
                                 
                                 PromocodeUsage::create([
                                    'user_id' => $request->user()->id,
                                    'promocode_id' => $find_promo->id,
                                    'status' => 'ADDED',
                                ]);
                            }
                            else{
                                return response()->json(['error' => trans('promo Amount invalid')], 422);
 
                            }
                           
                           
                        }
                      
                        }
                     
                }
                $total_net = $payable = ($total_net-$promocode_amount);
                // if($request->wallet){
                //     if($request->user()->wallet_balance > $total_net){
                //         $total_wallet_balance_left = $request->user()->wallet_balance - $total_net;
                        
                //         $total_wallet_balance = $total_net;
                //         $payable = 0;
                        
                //     }else{ 
                //         //$total_net = $total_net - $request->user()->wallet_balance;
                //         $total_wallet_balance = $request->user()->wallet_balance;
                //         if($total_wallet_balance >0){
                //             $payable = ($total_net - $total_wallet_balance);
                //         }
                //     }
                // }

                $Cart = [
                'delivery_charges' => Setting::get('delivery_charge', 0),
                'delivery_free_minimum' => Setting::get('delivery_free_minimum', 0),
                'tax_percentage' => Setting::get('tax', 0),
                'carts' => $CartItems,
                'total_price' => round($tot_price,2),
                'shop_discount' => round($discount,2),
                'tax' => round($tax,2),
                'promocode_amount' => round($promocode_amount,2),
                'promocode_id' => $promocode_id,
                'net' => round($total_net,2),
                'wallet_balance' => round($total_wallet_balance,2),
                'payable' => round($payable,2)
            ];
            return $Cart;
            }catch(Exception $e){

            }
        }
        $Cart = [
                'delivery_charges' => Setting::get('delivery_charge', 0),
                'delivery_free_minimum' => Setting::get('delivery_free_minimum', 0),
                'tax_percentage' => Setting::get('tax', 0),
                'carts' => $CartItems,
                'total_price' => $tot_price,
                'shop_discount' => $discount,
                'tax' => $tax,
                'promocode_id' => $promocode_id,
                'promocode_amount' => $promocode_amount,
                'net' => $total_net,
                'wallet_balance' => $total_wallet_balance,
                'payable' => $payable
            ];
            return $Cart;
        
        
    }
    public function promocode_add(Request $request)
    {
        $this->validate($request, [
            //'promocode_id' => 'exists:promocodes,id',
            'couponcode' => 'exists:promocodes,promo_code',
        ]);

        try{
            $CartItems = UserCart::list($request->user()->id);

            if($request->promocode_id==''){
                $find_promo = Promocode::where('promo_code',$request->couponcode)->first();
            }else{
                $find_promo = Promocode::find($request->promocode_id);
            }
//return PromocodeUsage::select('user_id')->where('promocode_id', $find_promo->id)->whereIN('status',['ADDED','USED'])->distinct()->count();
            // if($request->has('promocode_code') && !empty($request->promocode_code)){
                        
              
                            
            // }


            if($find_promo->status == 'EXPIRED' || (date("Y-m-d") > $find_promo->expiration)){

                if($request->ajax()){

                    return response()->json(['error' => trans('form.promocode.expired')], 422);
                } else {
                    return response()->json(['error' => trans('form.promocode.expired')], 422);
                }

            }  elseif(PromocodeUsage::select('promocode_id')->where('promocode_id', $find_promo->id)->where('status','ADDED')->first()){

                if($request->ajax()){

                    return response()->json(['error' => trans('Already Added in Cart !!')], 422);

                }else{
                    return response()->json(['error' => trans('Already Added in Cart !!')], 422);
                }
            }
            elseif(PromocodeUsage::select('user_id')->where('promocode_id', $find_promo->id)->whereIN('status',['ADDED','USED'])->distinct()->count() >= $find_promo->coupon_limit){

                if($request->ajax()){

                    return response()->json(['error' => trans('coupon_limit_excced')], 422);

                }else{
                    return back()->with('flash_error', trans('form.promocode.already_in_use'));
                }
            } elseif(PromocodeUsage::where('promocode_id', $find_promo->id)->where('user_id', $request->user()->id)->whereIN('status',['ADDED','USED'])->count() > $find_promo->coupon_user_limit){

                if($request->ajax()){

                    return response()->json(['error' => trans('form.promocode.already_in_use')], 422);

                }else{
                    return back()->with('flash_error', trans('form.promocode.already_in_use'));
                }

            } 
            else {
                $find_promo = Promocode::where('promo_code',$request->couponcode)->first();
                $promocode_id = $find_promo->id;
                $promocode_product_id = $find_promo->product_id;
                $promocode_amount = $find_promo->discount;

                // Session::put(['promocode_id' => $find_promo->id]);
                foreach($CartItems as $Product){
                    
                    if($promocode_product_id == $Product->product_id){
                        $price_without_promo = $Product->quantity * ProductPrice::where('product_id',$Product->product_id)->first()->orignal_price;

                        if($price_without_promo >= $promocode_amount){
                            if($find_promo->promocode_type=='amount'){
                                $promocode_amount = $find_promo->discount;
                            }else{
                                $promocode_amount = ($total_net*($find_promo->discount/100));
                            }

                        }
                        else{
                            return response()->json(['error' => trans('promo Amount invalid')], 422);

                        }
                        // else{
                        //     return response()->json(['error' => trans('promo Amount invalid')], 422);

                        // }
                        // if($find_promo->promocode_type=='amount'){
                        //     $promocode_amount = $find_promo->discount;
                        // }else{
                        //     $promocode_amount = ($total_net*($find_promo->discount/100));
                        // }
                        // PromocodeUsage::create([
                        //     'user_id' => $request->user()->id,
                        //     'promocode_id' => $find_promo->id,
                        //     'status' => 'ADDED',
                        // ]);
                        return response()->json([
                            'message' => trans('form.promocode.applied'),
                            'discount' => $promocode_amount,
                            'promocode' => $find_promo,
                        ]); 
                    }

                }
                return response()->json(['error' => 'Promocode does not matched !!'], 422);

                // if($request->has('check')){
                //     if($request->has('remove')){
                //         $request->session()->forget('promocode_id');
                //         $request->session()->forget('promocode_price');
                //         $request->session()->forget('promocode_name');
                //          return $find_promo;
                //     }else{
                //         //if($request->has('add')){
                //             $request->session()->put('promocode_id',$find_promo->id);
                //             $request->session()->put('promocode_price',$find_promo->discount);
                //             $request->session()->put('promocode_name',$find_promo->promo_code);
                //             return $find_promo;
                //         /*}else{

                //         }*/
                //     }
                // }else{
                    // $request->user()->wallet_balance += Promocode::find($find_promo->id)->discount;
                    // $request->user()->save();

                    // WalletPassbook::create([
                    //     'user_id' => $request->user()->id,
                    //     'amount'  => Promocode::find($find_promo->id)->discount,
                    //     'status' => 'CREDITED',
                    //     'message' => trans('form.promocode.message',['promocode' => $find_promo->promo_code])
                    // ]);

                    // PromocodeUsage::create([
                    //     'user_id' => $request->user()->id,
                    //     'promocode_id' => $find_promo->id,
                    //     'status' => 'USED',
                    // ]);

                    // return ['wallet_balance' => $request->user()->wallet_balance];

                    // if($request->ajax()){

                    //     return response()->json([
                    //         'message' => trans('form.promocode.applied')
                    //     ]); 

                    // } else {
                    //     // return back()->with('flash_success', trans('form.promocode.applied'));
                    //     return response()->json([
                    //         'message' => trans('form.promocode.applied')
                    //     ]); 
                    // }
                //}
            }

        } catch (Exception $e) {
            if($request->ajax()){
                return response()->json(['error' => trans('form.whoops')], 500);
            }else{
                return back()->with('flash_error', trans('form.whoops'));
            }
        }
    }
}
