@if (count($errors) > 0)
    <div class="alert alert-danger" style="padding-top: 35px; margin-top: 20px;">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if(Session::has('flash_error'))
    <div class="alert alert-danger" style="padding-top: 35px; margin-top: 20px;">
        <button type="button" class="close" data-dismiss="alert">×</button>
        {{ Session::get('flash_error') }}
    </div>
@endif

@if(Session::has('flash_failure'))
    <div class="alert alert-danger" style="padding-top: 35px; margin-top: 20px;">
        <button type="button" class="close" data-dismiss="alert">×</button>
        {{ Session::get('flash_failure') }}
    </div>
@endif


@if(Session::has('flash_success'))
    <div class="alert alert-success" style="padding-top: 35px; margin-top: 20px;">
        <button type="button" class="close" data-dismiss="alert">×</button>
        {{ Session::get('flash_success') }}
    </div>
@endif