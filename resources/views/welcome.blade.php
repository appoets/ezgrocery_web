<!DOCTYPE html>
<html lang="en">
<head>
  <title>EZ-Grocery</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap -->
  <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css')}}">
  <!-- FA icons -->
  <link rel="stylesheet" href="{{ asset('assets/css/all.min.css')}}">
  <!-- Styles -->
  <link rel="stylesheet" href="{{ asset('assets/css/style.css')}}">
  <link rel="stylesheet" href="{{ asset('assets/css/responsive.css')}}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.transitions.css">
  <link rel="shortcut icon" href="{{ Setting::get('site_favicon', asset('favicon.ico')) }}">
  <link rel="stylesheet"  type='text/css' href="{{ asset('assets/country/intl-tel-input/css/intlTelInput.min.css')}}" />
<style>
     .dz-preview .dz-image img{
          width: 100% !important;
          height: 100% !important;
          object-fit: cover;
      }
      
      .intl-tel-input{
          width: 100%;
          display: block !important;
      }
  </style>
</head>
<body>
@include('user.notification')
<header>
        <nav class="navbar navbar-expand-md navbar-dark top-header fixed-top">
            <div class="container">
                <a class="navbar-brand" href="{{url('/')}}"><img src="{{ asset(Setting::get('site_logo', 'logo.png')) }}" class="img-fluid" width="80%"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" >
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                   <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                       <li class="nav-item">
                            <a class="nav-link" href="#"><i class="fa fa-phone"></i>+1-647-977-3250</a>
                        </li>
                        @if(Auth::guest())
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-toggle="modal" data-target="#signup"><i class="fa fa-sign-in"></i>Sign Up</a>
                            </li>
                            <li class="nav-item">
                                <a class="btn btn-green px-4" href="#" data-toggle="modal" data-target="#signin1" style="padding: 3px 20px;">Login</a>
                            </li> 
                            <?php $cart_no =0; ?>
                        @else
                            <?php 
                            $cart = \App\UserCart::list(Auth::user()->id);
                            //dd($cart[0]->product->shop->name);
                            $cart_no = count($cart);?>
                            <li>

                            <li class="nav-item">
                                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user" aria-hidden="true"></i>
                                    <span>{{Auth::user()->name}}</span>
                                </a>
                               <!--  <div class="select_language">
                      <div class="dropdown text-xs-right ml-auto"> -->
                        
                         <div class="dropdown-menu">
                            <ul class="list-unstyled list-inline ct-topbar__list">
                            <ul class="list-unstyled ct-language__dropdown">

                               <!-- <li>
                                    <a href="{{url('/orders')}}">Profile</a>
                                </li> -->
                                <li>
                                    <a class="nav-link" href="{{url('/orders')}}">@lang('user.create.orders')</a>
                                </li>
                                <li>
                                    <a class="nav-link"  href="{{url('/offers')}}">@lang('user.create.offers')</a>
                                </li>
                                <li>
                                    <a class="nav-link" href="{{url('/payments')}}">@lang('user.create.payments')</a>
                                </li>
                                <li>
                                    <a class="nav-link" href="{{url('/favourite')}}">@lang('user.create.fav')</a>
                                </li>
                                <li>
                                    <a class="nav-link" href="{{url('/useraddress')}}">@lang('user.create.address')</a>
                                </li>
                                <li>
                                    <a class="nav-link" href="{{url('/referral')}}">Referral</a>
                                </li>
                                <li>
                                    <a class="nav-link" href="javascript:void(0);" onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    @lang('menu.user.logout')</a>
                                </li>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </ul>
                            </ul>
                            </div><!-- 
                            </div>
                            </div> -->
                        </li>
                        @endif
                        
                        <?php 
                           if($cart_no==0){
                                $url = url('store/details?name=');
                           }else{
                                $url = url('store/details/addcart/'.@$cart[0]->product->shop->name);
                            }
                            ?>
                        @if($cart_no > 0)
                        <li class="nav-item">
                            <a  class="nav-link px-4 btn-green"href="{{$url}}"><span class="cart-count">{{$cart_no}}</span> Cart&nbsp;<i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
                        </li>
                      
                        @endif
                            </li>
                        </ul>

                         <ul class="navbar-nav ml-auto">

                        <li class="nav-item pull-right float-right">

                        <a class=" nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><img src="/ca.png">

                            <span>Canada</aspan>
                        </a>

                        <!-- <div class="dropdown-menu">
                         <ul class="list-unstyled list-inline ct-topbar__list">
                            <ul class="list-unstyled ct-language__dropdown">
                              <li><a href="#" class="nav-link lang-en lang-select" data-lang="en">Canada</a></li>
                            </ul>
                         </ul>
                        </div> -->
                    </li>
                    </ul>


                    <div class="select_language">
                      <div class="dropdown text-xs-right ml-auto">
                        
                        
                        <!-- End of .dropdown-menu -->
                    </div>
                    </div>
                </div>
            </div>
        </nav>
</header>

  
@include('include.alerts')


<!--banner section-->
<section class="banner p-150">
      <div class="container">
        <div class="row pt-50 align-items-center">
          <div class="col-lg-6 col-md-12 col-xs-12">
            <div class="banner-text">
              <h1><span class="text-green">Grocery</span> Delivered from your selected store - <span class="text-red">Sameday</span></h1>
                <div class="search-box text-center">
                  <p class="pt-3">Enter your zip code and shop from your nearby store</p>
                  <!-- <form  action="{{url('stores')}}" id="my_map_form" >
                                 
                                 <div class="log-location-search input-group">
                                     <input type="text" id="pac-input" name="search_loc" class="form-control" placeholder="Enter Your Delivery Location" required autofocus>
                                     <span class="input-group-addon locate-me-btn my_map_form_current"><i class="ion-pinpoint"></i>  Locate Me</span>
                                     <span class="input-group-addon log-search-btn"><button>Find Product</button></span>
                                 </div>
                                 <input type="hidden" id="latitude" name="latitude" value="{{ old('latitude') }}" readonly >
                                 <input type="hidden" id="longitude" name="longitude" value="{{ old('longitude') }}" readonly >
                                 <div id="my_map"   style="height:500px;width:500px;display: none" ></div>
                             </form>
                             <form  action="{{url('stores')}}" id="my_map_form_current" >
                                 <input type="hidden" id="pac-input_cur" class="form-control search-loc-form" placeholder="Search for area,street name..." name="search_loc" value="{{ old('latitude') }}" >
                                 <input type="hidden" id="latitude_cur" name="latitude" value="{{ old('latitude') }}" readonly >
                                 <input type="hidden" id="longitude_cur" name="longitude" value="{{ old('longitude') }}" readonly >
                                 
                  </form>
                           -->

                  <form  action="{{url('stores')}}" id="my_map_form" >
                      <div class="input-group py-3 location-search">
                        <div class="findLocInputBox">
                          <input type="text" id="pac-input" name="search_loc" class="form-control location-box" required placeholder="Find my location">
                        </div>
                        <!-- <span class="input-group-addon locate-me-btn my_map_form_current"><i class="ion-pinpoint"></i>  Locate Me</span> -->
                        <div class="input-group-append">
                          <button class="btn btn-green px-4"> Find Stores&nbsp; <i class="fa  fa-long-arrow-right"></i></button>
                        </div>
                      </div>
                      <input type="hidden" id="latitude" name="latitude" value="{{ old('latitude') }}" readonly >
                      <input type="hidden" id="longitude" name="longitude" value="{{ old('longitude') }}" readonly >
                      <div id="my_map"   style="height:500px;width:500px;display: none" ></div>
                  </form>
                  <form  action="{{url('stores')}}" id="my_map_form_current" >
                      <input type="hidden" id="pac-input_cur" class="form-control search-loc-form" placeholder="Search for area,street name..." name="search_loc" value="{{ old('latitude') }}" >
                      <input type="hidden" id="latitude_cur" name="latitude" value="{{ old('latitude') }}" readonly >
                      <input type="hidden" id="longitude_cur" name="longitude" value="{{ old('longitude') }}" readonly >            
                  </form>
                  @if(!Auth::user())
                  <p class="py-1">Already have an Account?  <a class="text-green" href="#" data-toggle="modal" data-target="#signin1">Sign in</a></p>
                  @endif
               </div>
            </div>
          </div>
          <div class="col-lg-6 col-md-12 col-xs-12">
            <div class="baner-image">
              <img src="assets/images/baner-right.png" width="70%" alt=""  class="img-fluid">
            </div>
          </div>
        </div>
      </div>
</section>


<!-- Choose Section-->
  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-8 offset-md-2">
          <div class="section-title text-center">
            <img src="assets/images/title-icon.png" class="img-fluid title-img">
            <h2 class="text-uppercase">Why <span class="text-green">Choose Us</span></h2>
          </div>
        </div>
      </div>
      <div class="row pt-50">
        <div class="col-md-4 col-xs-12">
          <div class="choose-box">
            <img src="assets/images/choose-1.png" alt="" class="img-fluid">
            <h5 class="choose-title text-uppercase">Products you love</h5>
            <p class="description">Find 1,000's of products from the stores you already shop at.</p>
          </div>
        </div>

        <div class="col-md-4 col-xs-12">
          <div class="choose-box">
            <img src="assets/images/choose-2.png" alt="" class="img-fluid">
            <h5 class="choose-title text-uppercase">select your store for pickup or delivery</h5>
            <p class="description">We make deliveries in provinces like Ontario, Nunavut, Alberta, British Columbia, New Brunswick, Newfoundland and Labrador, Saskatchewan, Prince Edward Island, Quebec, Northwest Territories, Yukon, Nova Scotia.</p>
          </div>
        </div>

        <div class="col-md-4 col-xs-12">
          <div class="choose-box">
            <img src="assets/images/choose-3.png" alt="" class="img-fluid">
            <h5 class="choose-title text-uppercase">Save time & money</h5>
            <p class="description">Find exclusive deals on popular products — delivered to your front door!</p>
          </div>
        </div>
      </div>
    </div>

    <!-- Invite Section-->
  <div class="earn-section pt-5">
    <div class="container">
      <div class="row pt-50 d-flex align-items-center justify-content-between">
        <div class="col-md-7 col-xs-12">
          <div class="section-title">
            <img src="assets/images/title-icon.png" class="img-fluid title-img">
            <h2 class="text-uppercase">Invite more <span class="text-green">Earn More</span></h2>
            <p class="py-2">when you introduce your friends to place their first order</p>
            <a class="btn btn-green" href="#">Refer a Friend</a>
          </div>
        </div>
        <div class="col-md-5 col-xs-12">
            <img src="assets/images/refer.png" alt="" width="50%" class="img-fluid earn-img">
        </div>
      </div>
    </div>
  </div>
  </section>




<!-- Work Section-->
  <section class="works">
    <div class="container">
       <div class="row">
        <div class="col-md-8 offset-md-2">
          <div class="section-title text-center">
            <img src="assets/images/title-icon.png" class="img-fluid title-img">
            <h2 class="text-uppercase">How it <span class="text-green">Works</span></h2>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="works-list">
             <ul>
                <li>
                   <span>
                     <img src="assets/images/work-1.png">
                   </span>
                   <h6>Enter your location</h6>
                </li>
                <li>
                   <span>
                      <img src="assets/images/work-2.png">
                   </span>
                   <h6>Select Desired Store</h6>
                </li>
                <li>
                   <span>
                      <img src="assets/images/work-3.png">
                   </span>
                   <h6>Shop your Grocery</h6>
                </li>
                <li>
                   <span>
                      <img src="assets/images/work-4.png">
                   </span>
                   <h6>Select Delivery Option</h6>
                </li>
                <li>
                   <span>
                      <img src="assets/images/work-5.png">
                   </span>
                   <h6>Grocery at your Doorstep</h6>
                </li>
             </ul>
          </div>
      </div>
    </div>
  </section>

<!-- Free Pickup-->
<section class="pickup">
  <div class="container">
    <div class="row d-flex align-items-center justify-content-between">
      <div class="col-md-6 col-xs-12">
        <img src="assets/images/pickup.png" class="img-fluid pickup-img" style="width: 80%;">
      </div>
       <div class="col-md-6 col-xs-12 px-4">
         <div class="section-title">
            <img src="assets/images/title-icon.png" class="img-fluid title-img">
            <h2 class="text-uppercase"><span class="text-red">Free</span> <span class="text-green">Pickup</span></h2>
            <p class="py-2">No service and delivery charges if you pickup the grocery from the store you selected.</p>
            @if(Auth::guest())
                <?php $cart_no =0; ?>
                <a class="btn btn-green" href="#" data-toggle="modal" data-target="#signin1">Get Started</a>
            @else
              <?php 
                  $cart = \App\UserCart::list(Auth::user()->id);
                  //dd($cart[0]->product->shop->name);
                  $cart_no = count($cart);
                  if($cart_no==0){
                      $url = url('#');
                  }else{
                      $url = url('store/details/addcart/'.@$cart[0]->product->shop->name);
                  }
              ?>
           @endif
           @if(Auth::user())

            @if($cart_no > 0)

                <a class="btn btn-green" href="{{$url}}">Get Started</a>
            @else
              <a class="btn btn-green" href="#" onclick="alert('Please Add Items to Cart !!');">Get Started</a>

            @endif
          @endif

          </div>
      </div>
    </div>
  </div>
</section>


<!--Client Section-->
<section class="client-logo">
  <div class="container">
     <div class="customer-logos slider">
        <div class="slide"><img src="assets/images/client1.png"></div>
        <div class="slide"><img src="assets/images/client1.png"></div>
        <div class="slide"><img src="assets/images/client1.png"></div>
        <div class="slide"><img src="assets/images/client1.png"></div>
        <div class="slide"><img src="assets/images/client1.png"></div>
        <div class="slide"><img src="assets/images/client1.png"></div>
        <div class="slide"><img src="assets/images/client1.png"></div>
        <div class="slide"><img src="assets/images/client1.png"></div>
     </div>
  </div>
</section>


<!--Testimonial Section-->
<section class="testimonial">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="section-title text-center">
            <img src="assets/images/title-icon.png" class="img-fluid title-img">
            <h2 class="text-uppercase">Testimonials</h2>
            <p class="py-2">This is dummy copy. It is not meant to be read. It has been placed here solely to demonstrate the look and.</p>
          </div>
            <div id="testimonial-slider" class="owl-carousel">
                <div class="testimonial">
                    <p class="description">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis commodo nulla dictum felis sollicitudin, euismod finibus augue vulputate. Sed aliquam, elit eu gravida dignissim, justo dolor vulputate ipsum, a dapibus purus dui vitae purus. Maecenas massa arcu, rhoncus sit amet risus quis, porta hendrerit arcu. Pellentesque sagittis pretium nibh, et.
                    </p>
                    <div class="pic">
                        <img src="assets/images/avatar.png" alt="">
                    </div>
                    <h3 class="testimonial-title">
                        <span>williamson</span>
                        <small>Web Developer</small>
                    </h3>
                </div>
 
                <div class="testimonial">
                    <p class="description">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis commodo nulla dictum felis sollicitudin, euismod finibus augue vulputate. Sed aliquam, elit eu gravida dignissim, justo dolor vulputate ipsum, a dapibus purus dui vitae purus. Maecenas massa arcu, rhoncus sit amet risus quis, porta hendrerit arcu. Pellentesque sagittis pretium nibh, et.
                    </p>
                    <div class="pic">
                        <img src="assets/images/avatar.png" alt="">
                    </div>
                    <h3 class="testimonial-title">
                        <span>kristiana</span>
                        <small>Web Designer</small>
                    </h3>
                </div>
            </div>
        </div>
        <div class="col-md-6 testimonial-right">
          <div class="section-title">
            <img src="assets/images/title-icon.png" class="img-fluid title-img">
            <h2 class="text-uppercase">Get Delivery</h2>
            <h2 class="text-uppercase"><span class="text-green">Sameday</span></h2>
          </div>
          <div class="py-4">
            <p>No minimum order limit:</p>
            <p>Same day Delivery <strong>$6.99</strong></p>
            <p>Next day Delivery <strong>$3.99</strong></p>
            <p>Two day Delivery <strong>$2.99</strong></p>
          </div>
          @if(Auth::guest())
            <?php $cart_no =0; ?>
            <a class="btn btn-green" href="#" data-toggle="modal" data-target="#signin1">Get Delivery Now</a>
          @else
            <?php 
                $cart = \App\UserCart::list(Auth::user()->id);
                //dd($cart[0]->product->shop->name);
                $cart_no = count($cart);
                if($cart_no==0){
                    $url = url('#');
                }else{
                    $url = url('store/details/addcart/'.@$cart[0]->product->shop->name);
                }
            ?>
           @endif
           @if(Auth::user())
            @if($cart_no > 0)

              <a class="btn btn-green" href="{{$url}}">Get Delivery Now</a>
              @else
              <a class="btn btn-green" href="#" onclick="alert('Cart Empty !!');">Get Delivery Now</a>

              @endif
          @endif

        </div>
      </div>
    </div>
</section>

<!--Counter Section-->
<section class="counter">
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <div class="counter-box text-center text-white">
          <img src="assets/images/counter-1.png">
          <h4 class="py-2">Satisfied Customer</h4>
          <p class="nums">500+</p>
        </div>
      </div>
      <div class="col-md-3">
        <div class="counter-box text-center text-white">
            <img src="assets/images/counter-2.png">
            <h4 class="py-2">On Board Stores</h4>
            <p class="nums">50+</p>
          </div>
      </div>
      <div class="col-md-3">
         <div class="counter-box text-center text-white">
            <img src="assets/images/counter-3.png">
            <h4 class="py-2">Served Locations</h4>
            <p class="nums">100+</p>
          </div>
      </div>
      <div class="col-md-3">
        <div class="counter-box text-center text-white">
          <img src="assets/images/counter-4.png">
          <h4 class="py-2">Reviews</h4>
          <p class="nums">600+</p>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Part Section -->
<section>
  <div class="container">
    <div class="row text-center">
      <div class="col-md-12">
         <div class="section-title">
            <img src="assets/images/title-icon.png" class="img-fluid title-img">
            <h2 class="text-uppercase">Be a Part of <span class="text-green">EZ Grocery</span></h2>
            <p class="py-2">The pricing is same as calling the ezgrocery.ca or any grocery store directly.</p>
          </div>
      </div>
    </div>
    <div class="row pt-50">
      <div class="col-md-3">
        <div class="image-box">
          <img src="assets/images/img1.png">
          <h4 class="title">Retailer Onboard</h4>
          <p>Grow your business with<br> ezgrocery.</p>
          <a class="btn btn-green" href="{{url('/shop/register')}}">Sign Up</a>
        </div>
      </div>
      <div class="col-md-3">
        <div class="image-box">
          <img src="assets/images/img2.png">
          <h4 class="title">Wholesaler</h4>
          <p>Be a part of the largest digital market place.</p>
          <a class="btn btn-green" href="{{url('/shop/register')}}">Sign Up</a>
        </div>
      </div>
      <div class="col-md-3">
        <div class="image-box">
          <img src="assets/images/img3.png">
          <h4 class="title">Driver</h4>
          <p>Be more productive.</p>
          <!-- <a class="btn btn-green" style="margin-top:20px" href="#">Sign Up</a> -->
        </div>
      </div>
      <div class="col-md-3">
        <div class="image-box">
          <img src="assets/images/img4.png">
          <h4 class="title">Butcher</h4>
          <p>Grow your business with <br>ezgrocery.</p>
          <a class="btn btn-green" href="{{url('/shop/register')}}">Sign Up</a>
        </div>
      </div>
    </div>
  </div>
</section>








@include('user.layouts.partials.footer')


  <!-- Jquery -->
  <script src="{{ asset('assets/js/jquery.min.js')}}"></script>

  <!-- Bootstrap -->
  <script src="{{ asset('assets/js/popper.min.js')}}"></script>
  <script src="{{ asset('assets/js/bootstrap.min.js')}}"></script>

  <!-- Ionicons -->
  <script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>

  <!--slick carousel -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>

<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

<script type="text/javascript" src="{{ asset('assets/country/intl-tel-input/js/intlTelInput-jquery.min.js') }}"></script> 

 <!-- Map JS -->
 <script src="https://maps.googleapis.com/maps/api/js?key={{Setting::get('GOOGLE_API_KEY')}}"></script>
    <script src="{{ asset('assets/user/js/jquery.googlemap.js')}}"></script>
     <!-- Scripts -->
     <script src="{{ asset('assets/js/scripts.js')}}"></script>
  <script src="{{ asset('assets/user/js/scripts.js')}}"></script>
    @include('user.layouts.partials.script')
    @if(Setting::get('DEMO_MODE') == 0)

<script type="text/javascript">
     window.__lc = window.__lc || {};
     window.__lc.license = 8256261;
     (function() {
         var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
         lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
         var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
     })();
 </script> 
  @endif

  <script type="text/javascript">
    $(document).bind("contextmenu",function(e) {
 e.preventDefault();
});
$(document).keydown(function(e){
    console.log(e);
    if(e.which === 123){
       return false;
    }
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
     return false;
    }
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)) {
     return false;
    }
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
     return false;
    }
    if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
     return false;
    }
});
 setInterval(function(){
    var status = navigator.onLine ? 'online' : 'offline';
    if(status != 'online'){
      alert('Internet is not available. Please try again later.');
    }
    console.log(status);
}, 5000);
 
</script>

</body>
</html>
