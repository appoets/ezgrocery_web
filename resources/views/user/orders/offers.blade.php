@extends('user.layouts.app')

@section('content')
@include('user.layouts.partials.user_common')
<div class="container">
	<div class="">
		@include('user.layouts.partials.sidebar')
		<div class="tab-content mb-5">

			<div class="tab-pane container active" id="fav">
				<div class="container">
					<div class="row">
						<div class="fav-slider">
						<?php $existpromo=[]; ?>
							<!-- Coupon Box Starts -->
							@forelse($Promocodes as $promocode)
							<?php $used ='';?>
								@if(count($promocode->pusage)>0)
								<?php
										$existpromo[$promocode->id]=$promocode->pusage->pluck('promocode_id','user_id')->toArray();  ?>
								@else
								<?php $existpromo[$promocode->id]=[]; ?>
								@endif
								@if(count($existpromo[$promocode->id])>0)
								@if(array_key_exists(Auth::user()->id,$existpromo[$promocode->id]))
								<?php $used ='USED'; ?>
								@else
								<?php $used ='';  ?>
								@endif
								@endif

						

							<div class="item">

								<div class="favourites text-center m-2 p-4">
															
									<h5>shop: {{@$promocode->product->shop->name}}</h5>

									<h5>Discount: {{currencydecimal($promocode->discount)}}</h5>
									<h6>{{@$promocode->product->name}}</h6>
									<h5>Price: {{currencydecimal(@$promocode->product->prices->orignal_price)}}</h5>

									<p>Expires in
								{{date('d-m-Y',strtotime($promocode->expiration))}}</p>
								<h6 class="btn-green btn">Use Code: {{@$promocode->promo_code}}</h6>

									<div class="fav-bottom">

										<!-- <a href="#" class="btn btn-green">Order</a> -->
									</div>
								</div>
								

								{{$used}}
							</div>

							@empty
						<div>@lang('user.create.no_offers') </div>
						@endforelse

						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	</diiv>
	</div>














