@extends('user.layouts.app')

@section('content')
@include('user.layouts.partials.user_common')

<div class="container">
	<div class="">
		@include('user.layouts.partials.sidebar')
		<div class="tab-content mb-5">

			<div class="tab-pane container active" id="fav">
				<div class="container">
					<div class="row">
						<div class="fav-slider">
							@forelse($available as $item)
							@forelse(@$item->product as $key=>$products )

							<div class="item">
								<div class="favourites text-center m-2 p-4">
									@forelse(@$products->featured_images as $key=>$pro_image)

									<div class="product-img py-2">
										<img src="{{@$pro_image->url}}">

									</div>
									@empty
									@endforelse

									<h5>{{currencydecimal(@$products->prices->orignal_price)}}</h5>
									<h6>{{@$products->name}}</h6>
									<p>{{@$products->description}}</p>
									<div class="fav-bottom">



									<form action="{{Auth::guest()?url('mycart'):url('addcart')}}"
													method="POST">
													{{csrf_field()}}




													<!-- <label>Select Quantity</label> -->
													<input type="hidden"  value="{{@$products->shop_id}}" name="shop_id">
													<input type="hidden"  value="{{@$products->id}}"
														name="product_id">
													<input type="hidden" value="1" name="quantity" class="form-control"
														placeholder="Enter Quantity" readonly min="1" max="100">
													<input type="hidden"  value="{{@$products->name}}"
														name="name">
													<input type="hidden"  value="{{(@$products->prices->orignal_price)}}" name="price" />


													<!-- <button  class="add-btn">@lang('user.add_to_cart')</button> -->
                                                    @if(Auth::user())
                                                                @if(@$products->out_of_stock=='NO')
													<button class="btn-green btn" type="submit">Add to Cart </button>
                                                    @endif
                                                    @else

                                                                <a href="#" class="btn-green btn" data-toggle="modal" data-target="#signin1">Add to Cart </a>
                                                           
                                                            @endif
												</form>









										<!-- <a href="#" class="btn btn-green">Order</a> -->
									</div>
								</div>
							</div>

							@empty
							@endforelse
							@empty
							@endforelse

						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	</diiv>
	</div>

	@endsection