@extends('user.layouts.app')

@section('content')
<section class="profile-baner">
    <div class="container">
        <div class="row align-items pt-3">
            <div class="col-md-4">
                <div class="store-item text-center">
                    <img src="{{$Shop->avatar}}" class="img-fluid">
                    <div class="p-inner">
                        <h5>{{$Shop->name}}</h5>
                        <div class="cat">{{$Shop->maps_address}}</div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <form action="/product/search" method="get" class="expanding-search-form">
                    <div class="search-dropdown">
                        <!-- <a class="button dropdown-toggle" type="button">
                            <span class="toggle-active">Category</span>
                            <span class="ion-arrow-down-b"></span>
                        </a> -->

                        
                        <!-- <select class="dropdown-menu" name="cat" required>Category
                          <option value="Category" selected="">  <li><a href="#">Category</a></li> </option>
                           <option value="Produce"> <li><a href="#">Produce</a></li> </option>
                          <option value="Alchocol">  <li><a href="#">Alchocol</a></li> </option>
                           <option value="Fruits"> <li><a href="#">Fruits</a></li> </option>
                        </select> -->


                        <select  name="cat" required>
                                <option>Category</option>
                             @foreach($all_category as $cate)
                                 <option value="{{$cate->id}}">{{$cate->name}}</option>
                             @endforeach
                        </select>

                    </div>
                    <input class="search-input" name="shop" value="{{$Shop->id}}" type="hidden">
                    <input class="search-input" name="prodname"  id="global-search" type="text"  placeholder="Search">
                   <!--  <label class="search-label" for="global-search">
                        <span class="sr-only">Global Search</span>
                    </label>
                    <button class="button search-button" type="submit">
                        <span class="fa fa-search">
                        </span>
                    </button> -->
                     <button  type="submit" class="btn btn-green" style="border-radius: 0"><span class="fa fa-search"></span></button>
                </form>
            </div>
        </div>
    </div>
</section>
<div class="container baner-tab">
    <div class="row has-botom-border pt-2 align-items">
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#tabs-home" role="tab">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#tabs-dept" role="tab">Department</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#tabs-coupon" role="tab">Coupons</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#tabs-items" role="tab">Your Items</a>
            </li>
        </ul>
        <?php  $usr_location = Session::get('usr_loc'); ?>
        <p class="ml-auto">Delivery to <span class="text-green">{{$usr_location}}</span> 
            @if(count($Shop->timings) == 0)
            <span class="pl-3">Today Closed</span>
            @elseif(count($Shop->timings) == 1 && $Shop->timings[0]->day == 'ALL')
            <span class="pl-3">Today, {{date('h:i a ', strtotime($Shop->timings[0]->start_time))}} -
                {{date('h:i a ', strtotime($Shop->timings[0]->end_time))}}</span><span class="info-icon" data-toggle="popover" title="Shop Timing" data-content="Opening Time: {{date('h:i a ', strtotime($Shop->timings[0]->start_time))}} -Closing Time:
                {{date('h:i a ', strtotime($Shop->timings[0]->end_time))}}"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
             @else
             @foreach($Shop->timings as $time)
             @if($time->day == strtoupper(date('D')))
             @php $time_val = 0; @endphp
              <span class="pl-3">Today, {{date('h:i a ', strtotime($time->start_time))}} -
                {{date('h:i a ', strtotime($time->end_time))}}</span><span class="info-icon" data-toggle="popover" title="Shop Timing" data-content="Opening Time: {{date('h:i a ', strtotime($time->start_time))}} -Closing Time:
                {{date('h:i a ', strtotime($time->end_time))}}"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
                @break
                @else
                @php $time_val = 1; @endphp
             @endif
             @endforeach
               @if($time_val == 1)
                <span class="pl-3">Today Closed</span>
               @endif
            @endif
            </p>
    </div>
</div>

<section>
    <div class="tab-content">
        @forelse($category_list as $key=>$img)

        <div class="tab-pane active" id="tabs-home" role="tabpanel">
            <div class="container">
                <div class="product-outer">
                    <div class="carousel-top">
                        <div class="row align-items carousel-head">
                            <div class="col-6">
                                <h6><b>{{$img->name}}</b></h6>
                            </div>
                            <div class="col-6">
                                <div class=" d-flex align-items float-right">
                                   <!--  <ul id="tabs" class="nav nav-tabs">
                                        <li class="nav-item"><a href="" data-target="#home1" data-toggle="tab"
                                                class="nav-link small text-uppercase active">All</a></li>
                                        <li class="nav-item"><a href="" data-target="#profile1" data-toggle="tab"
                                                class="nav-link small text-uppercase ">Sale</a></li>
                                        <li class="nav-item"><a href="" data-target="#messages1" data-toggle="tab"
                                                class="nav-link small text-uppercase">Popular</a></li>
                                        <li class="nav-item"><a href="" data-target="#trends1" data-toggle="tab"
                                                class="nav-link small text-uppercase">Trend</a></li>
                                    </ul>
                                    <a href="#" class="view-link">View More<i class="fa fa-long-arrow-right"
                                            aria-hidden="true"></i></a> -->
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="product-carousel">
                        <div id="tabsContent" class="tab-content">
                            <div id="home1" class="tab-pane fade active show">
                                <div class="owl-carousel owl-theme">
                                    @forelse(@$img->products as $key=>$products )
                                  
                                    <div class="item">
                                        <div class="product-item">
                                            <div class="products text-center m-2 p-2">
                                                @forelse(@$products->images as $key=>$pro_image)

                                                <div class="overlay">
                                                    <?php  $img->id ?>
                                                    <h6 class="overlay-btn"><a href="#" class="product_details"
                                                            data-toggle="modal" data-name="{{@$products->name}}"
                                                            data-price="{{currencydecimal(@$products->prices->orignal_price)}}/each"
                                                            data-description="{{@$products->description}}"
                                                            data-img="{{@$pro_image->url}}" ,
                                                            data-shop_id="{{@$products->shop_id}}",
                                                            data-product_id="{{@$products->id}}",
                                                            data-pro_cart_price="{{(@$products->prices->orignal_price)}}",

                                                            data-directions="{{@$products->directions}}",
                                                            data-warnings="{{@$products->warnings}}",
                                                            data-details="{{@$products->details}}",
                                                            data-ingredients="{{@$products->ingredients}}",
                                                            data-view_more="{{@$products->id}}",
                                                            data-related_product="{{@$products->id}}",
                                                            data-category="{{@$img->id}}",
                                                            data-target="#myModal">View</a></h6>

                                                            <form action="{{Auth::guest()?url('mycart'):url('addcart')}}"
													method="POST">
													{{csrf_field()}}




													<!-- <label>Select Quantity</label> -->
													<input type="hidden"  value="{{@$products->shop_id}}" name="shop_id">
													<input type="hidden"  value="{{@$products->id}}"
														name="product_id">
													<input type="hidden" value="1" name="quantity" class="form-control"
														placeholder="Enter Quantity" readonly min="1" max="100">
													<input type="hidden"  value="{{@$products->name}}"
														name="name">
													<input type="hidden"  value="{{(@$products->prices->orignal_price)}}" name="price" />


													<!-- <button  class="add-btn">@lang('user.add_to_cart')</button> -->
                                                    @if(Auth::user())
                                                                @if(@$products->out_of_stock=='NO')
													<button class="btn-green btn" type="submit">Add to Cart </button>
                                                    @endif
                                                    @else
                                                    <!-- <a href="#" class="btn btn-green signup_link" >Add to Cart </a> -->
                                                    <a href="#" data-toggle="modal" data-target="#signin1" class="btn btn-green" >Add to Cart </a>

                                                    @endif
												</form>
                                                </div>

                                                <div class="product-img py-2">
                                                    <img src="{{@$pro_image->url}}">
                                                </div>
                                                @empty
                                                @endforelse
                                                <h5>{{currencydecimal(@$products->prices->orignal_price)}}</h5>
                                                <h6>{{@$products->name}}</h6>
                                                <p>{{@$products->description}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    @empty
                                    @endforelse
                                </div>
                            </div>
                            <!-- <div id="profile1" class="tab-pane fade">
                                <div class="owl-carousel owl-theme">
                                    <div class="item">
                                        <div class="product-item">
                                            <div class="products text-center m-2 p-2">
                                                <div class="overlay">
                                                    <h6 class="overlay-btn"><a href="#" data-toggle="modal"
                                                            data-target="#myModal">View</a></h6>
                                                </div>
                                                <div class="product-img py-2">
                                                    <img src="assets/images/fav1.png">
                                                </div>
                                                <h5>$10.0 /each</h5>
                                                <h6>Apple</h6>
                                                <p>This Dummy Text, not mean to Read</p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div> -->
                            <!-- <div id="messages1" class="tab-pane fade">
                                <div class="owl-carousel owl-theme">
                                    <div class="item">
                                        <div class="product-item">
                                            <div class="products text-center m-2 p-2">
                                                <div class="overlay">
                                                    <h6 class="overlay-btn"><a href="#" data-toggle="modal"
                                                            data-target="#myModal">View</a></h6>
                                                </div>
                                                <div class="product-img py-2">
                                                    <img src="assets/images/fav1.png">
                                                </div>
                                                <h5>$10.0 /each</h5>
                                                <h6>Apple</h6>
                                                <p>This Dummy Text, not mean to Read</p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="trends1" class="tab-pane fade">
                                <div class="owl-carousel owl-theme">
                                    <div class="item">
                                        <div class="product-item">
                                            <div class="products text-center m-2 p-2">
                                                <div class="overlay">
                                                    <h6 class="overlay-btn"><a href="#" data-toggle="modal"
                                                            data-target="#myModal">View</a></h6>
                                                </div>
                                                <div class="product-img py-2">
                                                    <img src="assets/images/fav1.png">
                                                </div>
                                                <h5>$10.0 /each</h5>
                                                <h6>Apple</h6>
                                                <p>This Dummy Text, not mean to Read</p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div> -->
                        </div>
                    </div>

                </div>
            </div>
        </div>
        @empty
        @endforelse
        <div class="tab-pane active" id="tabs-dept" role="tabpanel">
</section>


<!-- Modal Popup -->
<div class="modal" tabindex="-1" role="dialog" id="myModal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body  pt-5">
                <div id="default" class="padding-top0">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="simple-gallery">

                                <img class="maxi" src="#" id="product_image">

                               <div class="mini" id="product_image_dimension">
                                <!-- <img src="#" id="product_image_dimension"> -->
                                    <!--
                                    <img src="assets/images/fav1.png">
                                    <img src="assets/images/fav1.png">
                                    <img src="assets/images/fav1.png"> -->
                                </div>

                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="product-discription">
                                <h5 id="product_name"></h5>
                                <h6 class="text-green product_price"></h6>
                                <p id="product_decription"></p>
                            </div>
                            <div class="d-flex">
                            <form  action="{{Auth::guest()?url('mycart'):url('addcart')}}" method="POST" style="display: flex;">
                                    {{csrf_field()}}
                                    <div class="input-field">
                                    <input type="number" name="quantity" id="number" name="quantity" value="1" />
                                    <div class="countr-btn">
                                        <div class="value-button" id="decrease" onclick="decreaseValue()"
                                            value="Decrease Value">-</div>
                                        
                                        <div class="value-button" id="increase" onclick="increaseValue()"
                                            value="Increase Value">+</div>
                                        </div>


                               
                                    <!-- <label>Select Quantity</label> -->
                                    <input type="hidden" id="shop_id"value="" name="shop_id">
                                    <input type="hidden" id="pro_id" value="" name="product_id">
                                    <!-- <input type="hidden" value="1" name="quantity" class="form-control" placeholder="Enter Quantity" readonly min="1" max="100"> -->
                                    <input type="hidden" id="pro_name" value="" name="name">
                                    <input type="hidden" id="pro_price" value="" name="price" />

                                   </div>

                                     <!-- <button  class="add-btn">@lang('user.add_to_cart')</button> -->
                                     <!-- <button class="cart-btn btn btn-green" type="submit" >Add to Cart <i class="fa fa-shopping-cart"></i></button> -->




                                     @if(Auth::user())
                                        @if(@$products->out_of_stock=='NO')
                                        <button class="cart-btn btn btn-green" type="submit" >Add to Cart <i class="fa fa-shopping-cart"></i></button>
                                        @endif
                                    @else

                                        <!-- <a href="#" class="cart-btn btn btn-green signup_link_modal" >Add to Cart <i class="fa fa-shopping-cart"></i> </a> -->
                                        <a href="#" class="cart-btn btn btn-green" data-toggle="modal" data-target="#signin1" class="btn btn-green" >Add to Cart <i class="fa fa-shopping-cart"></i>  </a>
              
                                    @endif

                                 
                                    <!-- <a href="#" class="login-item add-btn"
                                        onclick="$('#login-sidebar').asidebar('open')">@lang('user.add_to_cart')</a> -->

                                    <!-- <a href="#" class="login-item add-btn" data-toggle="modal" data-target="#signin1">@lang('user.add_to_cart')</a> -->

                                </form>

                                <!-- <a class="fav-btn" href="#"><i class="fa fa-heart"></i></a> -->

                               
                                    <!-- <div class="fav-icon">
                                        <div class="icon-wishlist">
                                       
                                        </div>
                                    </div> -->
                                    
                                    @if(Auth::guest())

                                    @else
                                    @if(isset($products))
                                    <div class="fav-icon shopfav " id="{{@$products->id}}">
                                        @if(\App\Favorite::where('product_id',$products->id)->where('user_id',Auth::user()->id)->count()>0)
                                            <div class="icon-wishlist"></div>
                                        @else
                                        <div class="icon-wishlist in-wishlist"></div>
                                        @endif
                                    </div>
                                    @endif
                                   <!--  <div class="fav"></div> -->
                                    @endif
                                    
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="product-tab">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#tabs-1"
                                            role="tab">Details</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab">Ingrediants</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tabs-3" role="tab">Directions</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tabs-4" role="tab">Warnings</a>
                                    </li>
                                </ul><!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tabs-1" role="tabpanel">
                                        <p id="product_detail"></p>
                                    </div>
                                    <div class="tab-pane" id="tabs-2" role="tabpanel">
                                        <p id="product_ingredient"></p>
                                    </div>
                                    <div class="tab-pane" id="tabs-3" role="tabpanel">
                                        <p id="product_direction"></p>
                                    </div>
                                    <div class="tab-pane" id="tabs-4" role="tabpanel">
                                        <p id="product_warning"></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class=" has-botom-border d-flex">
                                <h6><b>Related Products</b></h6>
                                <p class="ml-auto">
                                    <a href="/store/details?name={{$Shop->name}}" class="view-link">View More<i class="fa fa-long-arrow-right"
                                            aria-hidden="true"></i></a>
                                </p>
                            </div>
                        </div>

                        <div class="row search-content p-3" id="related_pro">
                            
                           <!--  <div class="col-sm-3">
                                <div class="item">
                                    <img src="assets/images/store.png" class="img-fluid">
                                    <div class="p-inner">
                                        <h5>$1.50 / L</h5>
                                        <h6>Milk</h6>
                                        <div class="cat">This is Dummy title of the above store</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="item">
                                    <img src="assets/images/store.png" class="img-fluid">
                                    <div class="p-inner">
                                        <h5>$1.50 / L</h5>
                                        <h6>Milk</h6>
                                        <div class="cat">This is Dummy title of the above store</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="item">
                                    <img src="assets/images/store.png" class="img-fluid">
                                    <div class="p-inner">
                                        <h5>$1.50 / L</h5>
                                        <h6>Milk</h6>
                                        <div class="cat">This is Dummy title of the above store</div>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <!-- End Modal Popup -->

@endsection

@section('styles')
<style type="text/css">
    .countr-btn {
        display: block;
    }
    .input-field {
        display: flex;
    }
    input#number, input#number1 {
    text-align: center;
    border: none;
    border: 1px solid #ddd;
    /* border-bottom: 1px solid #ddd; */
    margin: 0px;
    width: 40px;
    height: 50px;
    padding-top: 0px;
}
form #decrease, form #increase {
    border-radius: 0 !important;
    margin-right: 0;
    margin-left: 0;
}

.value-button {
    display: block;
    height: 25px;
}
.cart-btn {
    padding: 10px 25px;
    font-size: 16px;
}
.fav-icon {
    background: red;
    margin-left: 10px;
    padding: 5px 7px;
    border-radius: 5px;
    align-items: center;
    /* vertical-align: middle; */
    /* top: 10px; */
    display: flex;
    width: 40px;
    justify-content: center;
}
.modal .search-content .overlay-btn {
        left: 30%;
}
.modal .overlay button.btn-green {
        left: 6% !important;
}
</style>
@endsection

@section('deliveryscripts')
    <script type="text/javascript">

    $(document).ready(function () {
     
         $('body').on('click', 'a.signup_link', function() {
            $("#myModal").hide()
            $("#signup").show();
        });
    });
</script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#myModal').on('show.bs.modal', function (e) {

                var product_name = $(e.relatedTarget).attr('data-name');
                var product_price = $(e.relatedTarget).attr('data-price');
                var product_decription = $(e.relatedTarget).attr('data-description');
                var img = $(e.relatedTarget).data('img');
                var shop_id = $(e.relatedTarget).attr('data-shop_id');
                var product_id = $(e.relatedTarget).attr('data-product_id');
                var product_cart_price = $(e.relatedTarget).attr('data-pro_cart_price');

                var product_details = $(e.relatedTarget).attr('data-details');
                var product_directions = $(e.relatedTarget).attr('data-directions');
                var product_warnings= $(e.relatedTarget).attr('data-warnings');
                var product_ingredients = $(e.relatedTarget).attr('data-ingredients');

                
                $(this).find('#product_name').text(product_name);
                $(this).find('.product_price').text(product_price);
                $(this).find('#product_decription').text(product_decription);
                $("#product_image").attr("src", img);

                $(this).find('#product_detail').text(product_details);
                $(this).find('#product_direction').text(product_directions);
                $(this).find('#product_warning').text(product_warnings);
                $(this).find('#product_ingredient').text(product_ingredients);

                //addcart
                $("#pro_name").val( product_name );
                $("#pro_price").val( product_cart_price );
                $("#shop_id").val( shop_id );
                $("#pro_id").val( product_id );

                var rel= '';

                var pro_dimension= '';

                var product_cat = $(e.relatedTarget).attr('data-category');

                $.ajax({
                      url: "/related/product",
                      type: "get",
                      data: { 
                        shop_id: shop_id, 
                        category_id: product_cat, 
                      },
                      success: function(response) {
                        console.log(response);
                    $.each( response , function( key, value ) { 

                    $.each( value.products , function( src, dst ) { 

                        $.each( dst.featured_images , function( pro, img ) { 
                            // <a class="overlay-btn" href="#"  data-toggle="modal" data-name='+dst.name+' data-img='+img.url+' data-price='+dst.prices.orignal_price+' data-description='+dst.description+' data-shop_id='+dst.shop_id+' data-product_id='+dst.id+' data-pro_cart_price='+dst.prices.orignal_price+' data-directions='+dst.directions+' data-warnings='+dst.warnings+' data-details='+dst.details+' data-ingredients='+dst.ingredients+' data-view_more='+dst.id+' data-related_product='+dst.id+' data-category='+value.id+' data-target="#myModal">View</a>\

                    rel += '<div class="col-md-3">\
                                <div class="item">\
                                <div class="overlay">\
								  <form action="{{Auth::guest()?url('mycart'):url('addcart')}}"method="POST">\
													{{csrf_field()}}\
													<input type="hidden"  value='+dst.shop_id+' name="shop_id">\
													<input type="hidden"  value='+dst.id+' name="product_id">\
													<input type="hidden" value="1" name="quantity" class="form-control" placeholder="Enter Quantity" readonly min="1" max="100">\
													<input type="hidden"  value='+dst.name+' name="name">\
													<input type="hidden"  value='+dst.prices.orignal_price+' name="price" />\
													<!-- <button  class="add-btn">@lang('user.add_to_cart')</button> -->\
													<button class="btn-green btn" type="submit">Add to Cart </button>\
												</form>\
					          </div>';


                        rel +=   '<img src='+img.url+' class="img-fluid">';
                    });

                    rel +=   '<div class="p-inner">\
                                            <h5>{{Setting::get('currency')}}'+dst.prices.orignal_price+'</h5>\
                                            <h6>'+dst.name+'</h6>\
                                            <div class="cat">'+dst.description+'</div>\
                                        </div>\
                                </div>\
                            </div>';
                   
                    });
                });
                    $('#related_pro').html(rel);
                      },
                      error: function(xhr) {
                        //Do Something to handle error
                      }
                });
                //product image dimension
                $.ajax({
                      url: "/product/imageDimension",
                      type: "get",
                      data: { 
                        shop_id: shop_id, 
                        category_id: product_cat, 
                        product_id: product_id
                    },
                      success: function(response) {
                        console.log(response);
                        $.each( response , function( key, value ) { 

                            $.each( value.products , function( src, dst ) { 

                                $.each( dst.images , function( pro, img ) { 

                                 
                                        console.log(img.url);

                                    
                                        pro_dimension += '<img src='+img.url+'>';
                                        // $("#product_image_dimension").attr("src", img.url);


                    
                                });
                            });
                        });
                        $('#product_image_dimension').html(pro_dimension);

                    },
                    error: function(xhr) {
                        //Do Something to handle error
                    }
                });





            });
        });


       //   $('.search-button').on('click', function(e) {
       //     e.preventDefault(); 
       //    alert('9');
       //     var prod_search = $("#pro_value").val();
       //     alert(prod_search);
       //     $.ajax({
       //         type: "get",
       //         url: '/product/search',
       //         data: {prodname:prod_search},
       //         success: function( msg ) {
       //             alert( msg );
       //         }
       //     });
       // });
       $('.shopfav').on('click',function(){
        var product_no =$(this).attr('id');
        console.log(product_no);

    if($(".shopfav div" ).hasClass( "in-wishlist")){
        console.log('if');
        var url = "{{url('favourite')}}/"+product_no;
        var method = 'DELETE';
    }else{
        console.log('else');
        var url = "{{url('favourite')}}";
        var method = 'POST';
    }
    console.log(url);
    console.log(method);
  //  alert(product_no);
    $.ajax({
        url: url,
        type:method,
        data:{'shop_id':{{$Shop->id}},'product_id':product_no,'_token':"{{csrf_token()}}"},
        success: function(res) { 
            if(method=='POST'){
                $('.shopfav i').addClass('active');
                $('.shopfav i').removeClass('ion-ios-heart-outline');
                $('.shopfav i').addClass('ion-ios-heart');
                $('.fav').html("Favourited");
            }else{
                $('.shopfav i').removeClass('active');
                $('.shopfav i').addClass('ion-ios-heart-outline');
                $('.shopfav i').removeClass('ion-ios-heart');
                $('.fav').html("Favourite");
            }
        },
        error:function(jqXhr,status){ 
            if( jqXhr.status === 422 ) {
                $(".print-error-msg").show();
                var errors = jqXhr.responseJSON; 

                $.each( errors , function( key, value ) { 
                    $(".print-error-msg").find("ul").append('<li>'+value[0]+'</li>');
                });
            } 
        }
    });
});

@if(isset($Cart['carts']))
    @if(count($Cart['carts'])==0)

    @elseif(@$Cart['carts'][0]->product->shop_id != $Shop->id)

            if(confirm("@lang('user.roster_change_msg')")){
                clearCart();
            }else{
                $('#my_map_form').submit();
                //window.location.href="{{url('/restaurants')}}";
            }
          console.log(1);   
    @endif 
@elseif(count($Cart) != 0)
        @if(!array_key_exists($Shop->id,$Cart))
            if(confirm("@lang('user.roster_change_msg')")){
                /*window.location.href="{{url('/restaurants')}}";*/
                clearCart();
            }else{
                $('#my_map_form').submit();
                //window.location.href="{{url('/restaurants')}}";
            }
            console.log(45);
        @endif

@endif

function clearCart(){
    $.ajax({
        url: "{{url('/clear/cart')}}",
        type:'GET',
        success: function(data) { 
            
        },
        error:function(jqXhr,status){ 
            if( jqXhr.status === 422 ) {
                $(".print-error-msg").show();
                var errors = jqXhr.responseJSON; 

                $.each( errors , function( key, value ) { 
                    $(".print-error-msg").find("ul").append('<li>'+value[0]+'</li>');
                });
            } 
        }
    });
}
</script>
<script>
$(document).ready(function(){
  $('[data-toggle="popover"]').popover();   
});
</script>
@endsection