@extends('user.layouts.app')

@section('content')

<section>
	<div class="container">
		<div class="row pt-5">
			<div class="col-md-12 pt-5">
				<div class="table-responsive">
					<table class="table table-cart text-center">
					<tbody>
						<tr>
							<th></th>
							<th>Image</th>
							<th>Product</th>
							<th>Price</th>
							<th>Quantity</th>
							<th>Total</th>
						</tr>
                        <?php $tot_gross=0;
                        $tot_gross_discount=0;
                        $add_promocode= '';
                        ?>
                        @forelse($Cart['carts'] as $item)
						<tr>


							<td class="vl-m text-red">
							<form action="{{ route('updatecart.destroy', $item->id) }}" method="POST">
                            	{{ csrf_field() }}
                                <input type="hidden" name="_method" value="DELETE">
                                <button class="btn btn-cancel" onclick="return confirm('Are you sure?')"><i style="color:#e4394a" class="fa fa-close fa-1x"></i></button>
                            </form>
							</td>



							<td><img src="{{@$item->product->images[0]->url}}" style="max-width:20%;" class="img-fluid"></td>
							<td>{{@$item->product->name}}</td>


							<td>
                            {{currencydecimal(@$item->product->prices->orignal_price)}}/each
							</td>
							<td>
								<button class="cart-add-btn">
									<div class="numbers-row" data-id="{{@$item->id}}" data-pid="{{@$item->product->id}}">
									<input type="number" min="1" data-price="{{@$item->product->prices->orignal_price}}" name="add-quantity" class="add-sec" id="add-quantity_{{$item->id}}" value="{{$item->quantity}}">

									</div>
								</button>
								
							</td>

                            <td>
                            <?php $tot_gross += @$item->quantity*@$item->product->prices->orignal_price; 
                              if($item->promocode_id){
                                $find_promo = \App\Promocode::find($item->promocode_id);
                                $add_code = $find_promo->promo_code;
                                $add_promocode .= ',' .$add_code;
                                //array_push($add_promocode, $add_code);
                                if($find_promo->promocode_type=='amount'){
                                $tot_gross_discount += $find_promo->discount;
                                }else{
                                $tot_gross_discount += number_format(( ($tot_gross*($find_promo->discount/100))),2,'.','');;
                                }
                            }
                            ?>
                                                <p class="total_product_{{@$item->id}}">{{currencydecimal(@$item->quantity*@$item->product->prices->orignal_price)}}</p>
                            </td>
                                                
                           
						@empty
                        </tr>
                        <tr><td colspan="2">@lang('user.empty_cart')</td></tr>
                        @endforelse
						<tr>
							<td colspan="6">
                            <div class="input-group">
                                    <span class="success message" ></span>
                                </div>
                                <!-- <form> -->
								<input type="text" name="couponcode" id="couponcode_apply" class="coupon-box">
                                <a href="#" class="btn btn-green appcupon_apply">Apply Coupon</a>
								<button disabled="true" class="btn btn-green float-right added_coupon_code">Update Cart</button>
                               
                                <!-- </form> -->
							</td>
                           
						</tr>
                     
					</tbody>
					</table>
                    <div style="display: flex; justify-content: flex-end">
                                    <span class="success update_message" ></span>
                                </div>
				</div>
              
				<div class="col-md-6 offset-md-6 pt-3">
					<h4 class="py-3">Cart Total</h4>
					<div class="table-responsive">
						<table class="table table-bordered">
							<tr>
								<th>Sub Total</th>

                                <td class="sub-total">{{currencydecimal(@$tot_gross)}}</td>

							</tr>

                                                <?php
                                                   
                                                    $net = $tot_gross;

                                                 ?>
        





					
                            <?php
                                                    
                                                    $tax = number_format($net*(Setting::get('tax')/100),2,'.','');
                                                    $withoutpromo_net = $net = number_format(($net+$tax),2,'.','');
                                                    if(Request::session()->get('promocode_id')){
                                                        $find_promo = \App\Promocode::find(Request::session()->get('promocode_id'));
                                                        if($find_promo->promocode_type=='amount'){
                                                        $net = $net-Request::session()->get('promocode_price');
                                                        }else{
                                                        $net = number_format(($net - ($net*($find_promo->discount/100))),2,'.','');;
                                                        }
                                                    }
                                                 ?>
                                        
                            <tr>
                                <th>( - ) Coupon code</th>
                                <td class="">{{$add_promocode}}</td>
                            </tr> 
                           
                            <?php
                                           $net = $tot_gross;
                                            $pay = number_format($net,2,'.','');

                                        ?>
							<tr>
								<th>Total</th>
                                <!-- <td  id="total_payable" class="to_pay">{{currencydecimal($tot_gross_discount)}}</td> -->
                                <td>{{currencydecimal($tot_gross - $tot_gross_discount)}}</td> 

							</tr>
							
						</table>
					</div>
				</div>
			</div>
		</div>
        <input type="hidden"  id="total_amount_pay" value="{{$pay}}" />

        <input type="hidden" id="payable_tax" value="{{$tax}}"/>
        <input type="hidden" id="total_price" value="{{$tot_gross}}"/>
		<input type="hidden" id="total_addons_price" value="0"/>
		<input type="hidden" id="total_product_price" value="{{$tot_gross}}"/>
		<div class="row">
			<div class="col-md-12">
				<!-- <a href="#" class="btn btn-green float-right">Proceed to Checkout</a> -->
				<a href="{{url('store/details')}}?name={{$Shop->name}}&myaddress=home"  id="cart_add" class="btn btn-green float-right">Proceed to Checkout <i class="ion-ios-arrow-thin-right"></i></a>

			</div>
		</div>
       
	</div>
</section>

@endsection

    @section('styles')
    <style type="text/css">
        /* .active {
            background-color: Pink;
            :
		} */
		.cart-add-btn {
    padding: 0px 25px;
    line-height: inherit;
    border: 1px solid #d4d5d9;
    position: relative;
    height: 30px;
    background: #fff;
    font-size: 12px;
}
.add-sec {
    width: 30px;
    height: 28px;
    border: 0px;
    text-align: center;
}
.inc {
    position: absolute;
    top: 6px;
    right: 0px;
    text-align: center;
    width: 20px;
    height: 28px;
}

.dec {
    position: absolute;
    top: 6px;
    left: 0px;
    text-align: center;
    width: 20px;
    height: 28px;
}

    </style>
    @endsection
	@section('scripts')
<!-- <script type="text/javascript" src="{{asset('assets/user/js/max-bootstrap.min.js')}}"></script> -->
<script type="text/javascript">
$(document).ready(function() {   


$('.added_coupon_code').on('click',function(){
   var cop_value =  $('#couponcode_apply').val();


   $.ajax({
                    url: "{{url('/promocode/update')}}",
                    type: "get",
                    data:{'promocode_code':cop_value,'_token':"{{csrf_token()}}"}
                        })
                    .done(function(response){ 
                        console.log(response);
                        // $('.dis_count').html("{{Setting::get('currency')}}"+response.promocode_amount);

                        var location= "{{url('store/details')}}?name={{$Shop->name}}&myaddress=home";
                        $("#cart_add").attr("href", location);



                       
                    })
                    .fail(function(jqXhr,status){
                        if(jqXhr.status === 422) {
                            error =1;
                           
                            var errors = jqXhr.responseJSON;
                            console.log(errors);
                            $.each( errors , function( key, value ) { 
                                //  $('.message').html(value);
                            }); 
                        } 
                    
                        $('#ord_btn').prop('disabled',true);
                    });
                    location.reload(true);

});

});



$(document).ready(function() {   

$("input[type='radio']").change(function () {
        var selection=$(this).val();
        var total_product_payable = parseFloat($('#payable_tax').val())+parseFloat($('#total_product_price').val())+parseFloat(selection);
        $('#total_payable').html("{{Setting::get('currency')}}"+total_product_payable);

    });
   
$(document).on('click','.inc',function(e){
    e.preventDefault();
    $("input[type='radio']").prop("checked", false);

    var id = $(this).parent().attr('data-id'); 
    var pid = $(this).parent().attr('data-pid');   
    var input = $("input[id='add-quantity_"+id+"']");
    var currentVal = parseFloat(input.val());
    var tips_val = $('input[type="radio"]:checked').val();
    //alert(tips_val);

    if (!isNaN(currentVal)) {
        product_price_calculation(id,'plus');
        changeCart(id,pid,currentVal,tips_val);
    } else {
        input.val(0);
    }
});
$(document).on('click','.dec',function(e){
    e.preventDefault();
    $("input[type='radio']").prop("checked", false);
    var id = $(this).parent().attr('data-id');
    var pid = $(this).parent().attr('data-pid');    
    var input = $("input[id='add-quantity_"+id+"']");
    var currentVal = parseFloat(input.val());
    var tips_val = $('input[type="radio"]:checked').val();
    if (!isNaN(currentVal)) {
        if(currentVal==0){ 
            changeCart(id,pid,currentVal,tips_val);
        }else{
        product_price_calculation(id,'minus'); 
        changeCart(id,pid,currentVal,tips_val);  
        } 
    } else {
        input.val(0);
    }
});

  function product_price_calculation(val,type){

    if(type == 'plus'){
      var qty = $('#add-quantity_'+val).val();

      var price = $('#add-quantity_'+val).data('price');
      var tot_amt = qty*price;
      $('.total_product_'+val).html("{{Setting::get('currency')}}"+tot_amt.toFixed(2));
      ///
      var total = parseFloat(price)+parseFloat($('#total_price').val());
      
      var total_product_price = parseFloat($('#total_product_price').val())+parseFloat(price);
      $('#total_product_price').val(total_product_price);
      var total_addons_price = $('#total_addons_price').val();
      total = parseFloat(total_product_price)+qty*parseFloat(total_addons_price);
      $('#total_price').val(total); 
      $('.sub-total').html("{{Setting::get('currency')}}"+total.toFixed(2));
    }else{
      var qty = $('#add-quantity_'+val).val();

      var price = $('#add-quantity_'+val).data('price');
      var tot_amt = qty*price;
      $('.total_product_'+val).html("{{Setting::get('currency')}}"+tot_amt.toFixed(2));
      ///
      var total = parseFloat(price)+parseFloat($('#total_price').val());
      
      var total_product_price = parseFloat($('#total_product_price').val())-parseFloat(price);
      $('#total_product_price').val(total_product_price);
      var total_addons_price = $('#total_addons_price').val();
      total = parseFloat(total_product_price)+qty*parseFloat(total_addons_price);
      $('#total_price').val(total.toFixed(2)); 
      $('.sub-total').html("{{Setting::get('currency')}}"+total.toFixed(2));
    }
}
});
function changeCart(id,pid,qty,tip){
    $.ajax({
        url: "{{url('addcart')}}",
        type:'POST',
        data:{'cart_id':id,'quantity':qty,'_token':"{{csrf_token()}}",'product_id':pid,'tips':tip},
        success: function(res) { 
            console.log(res);
            $('.to_pay').html("{{Setting::get('currency')}}"+res.net);
            $('.tax_net').html("{{Setting::get('currency')}}"+res.tax);
            location.reload(true);
            if(qty==0){
            location.reload();
            }
        },
        error:function(jqXhr,status){ 
            if( jqXhr.status === 422 ) {
                $(".print-error-msg").show();
                var errors = jqXhr.responseJSON; 

                $.each( errors , function( key, value ) { 
                    $(".print-error-msg").find("ul").append('<li>'+value[0]+'</li>');
                });
            } 
        }
    });
}
$('.shopfav').on('click',function(){
    if($(".shopfav i" ).hasClass( "active")){
        var url = "{{url('favourite')}}/{{$Shop->id}}";
        var method = 'DELETE';
    }else{
        var url = "{{url('favourite')}}";
        var method = 'POST';
    }
    $.ajax({
        url: url,
        type:method,
        data:{'shop_id':{{$Shop->id}},'_token':"{{csrf_token()}}"},
        success: function(res) { 
            if(method=='POST'){
                $('.shopfav i').addClass('active');
                $('.shopfav i').removeClass('ion-ios-heart-outline');
                $('.shopfav i').addClass('ion-ios-heart');
                $('.fav').html("Favourited");
            }else{
                $('.shopfav i').removeClass('active');
                $('.shopfav i').addClass('ion-ios-heart-outline');
                $('.shopfav i').removeClass('ion-ios-heart');
                $('.fav').html("Favourite");
            }
        },
        error:function(jqXhr,status){ 
            if( jqXhr.status === 422 ) {
                $(".print-error-msg").show();
                var errors = jqXhr.responseJSON; 

                $.each( errors , function( key, value ) { 
                    $(".print-error-msg").find("ul").append('<li>'+value[0]+'</li>');
                });
            } 
        }
    });
});
$('#veg-check').on('click',function(){
    if($(this).is(':checked')){
        $('#veg_check_form').submit();
    }else{
        $('#veg_check_form').submit();
    }
})
$('.prodsearch,.search-close').on('click',function(){
    $('#prod_search_form').submit();
})
document.onkeydown = function(evt) {
    evt = evt || window.event;
    if (evt.keyCode == 27) {
        $('#prod_search_form').submit();
    }
};




var exist_promocode = "{{Request::session()->get('promocode_id')?:''}}";
$('.appcupon_apply').click(function(){
    
    $("input[type=radio]").attr('disabled', true);
    if($('#couponcode_apply').val()!=''){
        var promocode_id = $('#couponcode_apply').data('id');
        var couponcode_apply = $('#couponcode_apply').val();
       // alert(couponcode_apply);
        // alert(exist_promocode);
        if(exist_promocode){
                if(exist_promocode==couponcode_apply){
                    $('.message').html('Couponcode Already Added ');
                    // alert('already');
                }else{
                    $('.message').html('Another Couponcode Already Added');
                }
                $("input[type=radio]").attr('disabled', false);
             //   var location= $('#cart_add').attr("href");
               // $("#cart_add").attr("href", location);

               // $("#cart_add").removeAttr('href');

        }else{
                $.ajax({
                    url: "{{url('/promocode/add')}}",
                    type: "GET",
                    data:{'promocode_id':promocode_id,'check':1,'couponcode':couponcode_apply,'_token':"{{csrf_token()}}"}
                        })
                    .done(function(response){ 
                        console.log(response);
                        exist_promocode = response.promocode.id;
                        // $('#promocode_id').val(promocode_id);
                        //  var price = $('#couponcode_apply').data('price')
                         var price = response.promocode.discount;
                        if(response.promocode.promocode_type=='amount'){
                            var tot_blnce = (parseFloat($('#total_amount_pay').val())-price).toFixed(2);
                        }else{
                            var tot_blnce = parseFloat(parseFloat($('#total_amount_pay').val()) -(parseFloat($('#total_amount_pay').val())*parseFloat(response.discount/100))).toFixed(2);
                        }
                         
                      //  $('#total_amount_pay').val(tot_blnce);
                        $('.to_pay').html(tot_blnce);
                        $('.message').html(response.message); 
                        $('.dis_count').html("{{Setting::get('currency')}}"+response.discount);
 
                        $(".added_coupon_code").attr('disabled', false);
                       
                        var location= $('#cart_add').attr("href");

                        $("#cart_add").removeAttr('href');
                        
                        $('.update_message').html('Please Update Cart');  


                     //   $("#cart_add").attr("href", location);

                    })
                    .fail(function(jqXhr,status){
                        if(jqXhr.status === 422) {
                            error =1;
                           
                            var errors = jqXhr.responseJSON;
                            console.log(errors);
                            $.each( errors , function( key, value ) { 
                                 $('.message').html(value);
                            }); 
                        } 
                    
                        $('#ord_btn').prop('disabled',true);
                    });
        }
    }else{
        $('.message').html('Please Use Any Coupon Code');
        $("input[type=radio]").attr('disabled', false);

    }
    
})

</script>


@endsection
