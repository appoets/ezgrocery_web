
<div class="modal" id="card_model" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered modal-dialog-zoom modal-md" role="document">
    <div class="modal-content">
      <div class="modal-body">
<form id="payment-form" action="{{ route('card.store') }}" method="POST">
    {{ csrf_field() }}
    <div class="pt-5">
        <div class="radio d-flex align-items-center">
            <input id="radio-1" name="brand" type="radio" value="master" checked>
            <label for="radio-1" class="radio-label"><img src="{{ asset('assets/images/login/master.png')}}" width="50%"></label>
            <input id="radio-2" name="brand" type="radio" value="visa">
            <label for="radio-2" class="radio-label"><img src="{{ asset('assets/images/login/visa.png')}}" width="50%"></label>
        </div>
    </div>
    <div class="pt-5" id="card-payment">
        <div class="row">
        <div class="form-group col-md-12 col-sm-12">
                <label>@lang('user.card.fullname')</label>
                <input name="name" autocomplete="off" required type="text" class="form-control" placeholder="@lang('user.card.fullname')">
            </div>
        <div class="form-group col-md-12 col-sm-12">
                <label>@lang('user.card.card_no')</label>
            <input class="form-control" autocomplete="off" required name="card" placeholder="Card Number"/>
        </div>
        <div class="form-group col-md-4 col-sm-12">
                <label>@lang('user.card.month')</label>
            <input class="form-control" autocomplete="off" required name="cardExp_month" placeholder="00"/>
        </div>
        <div class="form-group col-md-4 col-sm-12">
                <label>@lang('user.card.year')</label>
            <input class="form-control" autocomplete="off" required name="cardExp_year" placeholder="00"/>
        </div>
        <div class="form-group col-md-4 col-sm-12">
                <label>@lang('user.card.cvv')</label>
            <input class="form-control" autocomplete="off" required name="cardCVV" placeholder="123"/>
        </div>
       
        </div>
    </div>
    <div class="col-md-12 form-group">
        <button type="submit" class="btn btn-green float-right form-control">@lang('user.card.add_card')</button>

        <!-- <input type="submit" name="" class="btn btn-green float-right form-control" Value="Add Card"> -->
    </div>
</form>
</div>
</div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>


<script type="text/javascript">
   
    $('#payment-form').submit(function (e) {

       
    });
</script>
<script type="text/javascript">
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode != 46 && charCode > 31 &&
            (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    $('#card_pay').on('change', function () {

        if ($(this).is(':checked')) {
            $('$card_id').val($(this).val());
        }
    })

    $('#cardNumber').on('keyup', function (e) {
        var val = $(this).val();
        var newval = '';
        val = val.replace(/\s/g, '');
        for (var i = 0; i < val.length; i++) {
            if (i % 4 == 0 && i > 0) newval = newval.concat(' ');
            newval = newval.concat(val[i]);
        }
        $(this).val(newval);
    });
</script>