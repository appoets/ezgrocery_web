@extends('user.layouts.app')

@section('content')
@include('user.layouts.partials.user_common')

<div class="container">
	<div class="">
		@include('user.layouts.partials.sidebar')
		<div class="tab-content mb-5">

			<div class="tab-pane container active" id="address">
			
				<div class="row">
				<?php $add_type = ['home'=> 'Home','work'=> 'Work','other'=> 'Other']; ?>
						@forelse($user_address as $Address)

						<?php if($Address->type!='other'){
                                                unset($add_type[$Address->type]);
                            } ?>
					<div class="col-md-6">
						
						<div class="address-box p-5 br-10 box-shadow">
							<h5 class="text-green"><i class="fa fa-map-marker pr-2"></i>{{ucfirst($Address->type)}}</h5>
							<p class="address-inner ml-4">{{$Address->map_address}}</p>
							<p></p>
							<div data-id="{{$Address->id}}">
								<input type="hidden" value="{{$Address->latitude}}" class="latitude" />
								<input type="hidden" value="{{$Address->longitude}}" class="longitude" />
								<input type="hidden" value="{{$Address->type}}" class="type" />
								<input type="hidden" value="{{$Address->map_address}}" class="mapaddrs" />
								<input type="hidden" value="{{$Address->building}}" class="building" />
								<input type="hidden" value="{{$Address->landmark}}" class="landmark" />
							
								<a href="{{ route('useraddress.edit', $Address->id) }}" class="btn btn-green" >Edit</a>

								<button class="btn btn-green"
									onclick="return confirm('Do You want To Remove This Address?');"
									form="resource-delete-{{ $Address->id }}">Delete</button>

								<form id="resource-delete-{{ $Address->id }}"
									action="{{ route('useraddress.destroy',$Address->id)}}" method="POST">
									{{ csrf_field() }}
									{{ method_field('DELETE') }}
									<input type="hidden" value="{{$Address->id}}" name="banner_id" />
									<input type="hidden" value="{{$Address->status}}" name="status" />
								</form>
							</div>
						</div>
					</div>
					<!-- 
			  		<div class="col-md-6">
			  			<div class="address-box p-5 br-10 box-shadow">
			  				<h5 class="text-green"><i class="fa fa-map-marker pr-2"></i> Work</h5>
			  				<p class="address-inner ml-4">123 Road, ABC Street, Edison Avenue,<br> Baltimore, USA</p>
			  			</div>
			  		</div>  -->

					@empty

					@endforelse
					<!-- <div class="col-md-12 text-center add-address-row py-5">
						<a href="#" data-toggle="modal" data-target="#address_model"
							class="btn btn-green add-address">Add New Address</a>
					</div> -->
					<div class="col-md-12 text-center add-address-row py-5">
		  				<a href="#" class="btn btn-green add-address">Add New Address</a>
		  			</div>
				</div>
				<!-- <div class="modal" id="address_model" tabindex="-1" role="dialog">
					<div class="modal-dialog modal-dialog-centered modal-dialog-zoom modal-md" role="document">
						<div class="modal-content">
							<div class="modal-body">

								<div class="row address-form">
									<div class="col-md-6">
										<form action="{{route('useraddress.store')}}" method="POST" id="comon-form"
											class="common-form">
											{{ csrf_field() }}
											<div class="input-section">
												<div class="form-group">
													<label>Address</label>
													<input class="form-control addr-mapaddrs" id="pac-input"
														name="map_address" type="text"
														value="{{Session::get('search_loc')}}"
														placeholder="enter your address">
													<input type="hidden" id="latitude" name="latitude"
														value="{{ Session::get('latitude') }}" readonly required>
													<input type="hidden" id="longitude" name="longitude"
														value="{{ Session::get('longitude') }}" readonly required>
												</div>
												<div class="form-group">
													<label>Door / Flat no.</label>
													<input class="form-control addr-building" name="building"
														type="text" value="">
												</div>
												<div class="form-group">
													<label>Landmark</label>
													<input class="form-control addr-landmark" name="landmark"
														type="text">
												</div>
												<div class="form-group">
													<label>Address Type</label>
													<select class="form-control addr-type" name="type">
														@foreach($add_type as $key => $item)
														<option value="{{$key}}">{{$item}}</option>
														@endforeach
													</select>
												</div>
											</div>
											<button class="btn btn-green float-right">Save &amp; Proceed</button>
										</form>
									</div>
									<div class="col-md-6">
										<div class="" id="my_map" style="width: 100%; height: 200px;"></div>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div> -->

				<div class="row address-form">
		  			<div class="col-md-6">
					  <form action="{{route('useraddress.store')}}" method="POST">
						{{ csrf_field() }}			  				
							<div class="form-group">
			  					<!-- <input type="text" class="form-control" name="" placeholder="Address"> -->
								  <input class="form-control addr-mapaddrs" id="pac-input"
														name="map_address" type="text"
														value="{{Session::get('search_loc')}}"
														placeholder="Enter your address">
													<input type="hidden" id="latitude" name="latitude"
														value="{{ Session::get('latitude') }}" readonly required>
													<input type="hidden" id="longitude" name="longitude"
														value="{{ Session::get('longitude') }}" readonly required>
			  				</div>
			  				<div class="form-group">
			  					<input type="text" class="form-control" name="building" placeholder="Zip Code">
			  				</div>
			  				<div class="form-group">
			  					<input type="text" class="form-control" name="landmark" placeholder="Landmark">
			  				</div>
			  				<div class="form-group">
								<!-- <select class="form-control" id="" placeholder="Add Address Type">
								    <option selected>Home</option>
								    <option>Work</option>
								</select>	 -->
								<select class="form-control addr-type" name="type">
														@foreach($add_type as $key => $item)
														<option value="{{$key}}">{{$item}}</option>
														@endforeach
								</select>
							</div>
							<!-- <a class="btn btn-green float-right" href="#">Save Address </a> -->
							<button class="btn btn-green float-right">Save &amp; Proceed</button>
			  			</form>
			  		</div>
			  		<div class="col-md-6">
					  <div class="" id="my_map" style="width: 100%; height: 300px;"></div>

			  			<!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d387190.2798911863!2d-74.25986818535776!3d40.69767006766623!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew%20York%2C%20NY%2C%20USA!5e0!3m2!1sen!2sin!4v1586420167431!5m2!1sen!2sin" width="600" height="450" frameborder="0" style="border:0;" class="box-shadow br-10" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe> -->
			  		</div>
		  		</div>



			</div>
		</div>
	</div>
</div>
</div>

@endsection

@section('styles')
<style>
	.pac-container {
		z-index: 9999999999999999999 !important;
	}
</style>
@endsection
@section('scripts')
<script>
	$('.myaddress').on('click', function () {
		var id = $(this).parent().data('id');
		$('.addr-building').val($(this).parent().find('.building').val());
		$('.addr-landmark').val($(this).parent().find('.landmark').val());
		$('#latitude').val($(this).parent().find('.latitude').val());
		$('#longitude').val($(this).parent().find('.longitude').val());
		var type = $(this).parent().find('.type').val();
		if (type != 'other') {
			$(".addr-type").append("<option value='" + type + "'>" + capitalizeFirstLetter(type) + "</option>");
		}
		$(".addr-type option[value='" + type + "']").prop('selected', true);
		$('.addr-mapaddrs').val($(this).parent().find('.mapaddrs').val());
		$('#comon-form').attr('action', "{{url('useraddress')}}" + '/' + id);
		$('#comon-form').append('<input name="_method" value="PATCH" type="hidden">');
	})
</script>

@endsection