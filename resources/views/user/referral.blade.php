@extends('user.layouts.app')

@section('content')
<section class="profile-baner">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center mx-auto pt-5">
				<h4><b>Invite More <span class="text-green">Earn More</span></b></h4>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="refer-left">
					<div class="py-3 has-botom-border">
						<h6><b>$ @if(!empty($referrals[0]->total_amount)){{$referrals[0]->total_amount}}@else 0 @endif
						 Earned. $50 Remaining</b></h6>
					</div>
					<div class="refer-count py-3">
						<h4 class="text-green"><b>New Invites</b></h4>
						<h2 class="text-green"><b>(@if(!empty($referrals[0]->total_count)){{$referrals[0]->total_count}}@else 0 @endif )</b></h2>
						<div class="progress my-3">
						  <div class="progress-bar" role="progressbar" aria-valuenow="70"
						  aria-valuemin="0" aria-valuemax="100" style="width:5%; background: #3ec423">
						    <span class="sr-only">10% Complete</span>
						  </div>
						</div>
						<p class="py-3">Invite 20 or more friends to Increase your Choices..</p>
					</div>
				</div>
			</div>
			<div class="col-md-7 col-sm-7 col-xs-12">
				<div class="refer-right">
					<div class="refer-top text-center has-botom-border">
						<h4><b>Earn $10 for Every friend</b></h4>
						<p>When you Introduce your friends to place their first</p>
					</div>
					<div class="refer-social has-botom-border">
						<h5 class="text-green py-3">Share your Link</h5>
						<p class="d-flex bordered align-items">
							<span class="text-light small-text">https:ezgrocery/d/  @if(!empty(Auth::user()->referral_unique_id)){{Auth::user()->referral_unique_id}}@else -
                                @endif</span>
							<span class="ml-auto float-right btn-copy">Copy</span>
						</p>
						<p class="d-flex bordered align-items">
							<span class="text-light small-text">Code:  @if(!empty(Auth::user()->referral_unique_id)){{Auth::user()->referral_unique_id}}@else -
                                @endif</span>
							<span class="ml-auto float-right btn-copy">Copy</span>
						</p>
						<div class="social-icon py-3">
							<h6><b>Share on:</b> <a class="" target="_blank" href="https://www.facebook.com/share?url"><i
                                                class="fa fa-facebook f-bg" aria-hidden="true"></i></a>

												<a class="" target="_blank" href="https://www.twitter.com/share?url"><i
                                                class="fa fa-twitter t-bg" aria-hidden="true"></i></a>

												<a class="" target="_blank" href="https://www.instagram.com/share?url"><i
                                                class="fa fa-instagram i-bg" aria-hidden="true"></i></a>

												<a class="" target="_blank" href="https://www.linkedin.com/share?url"><i
                                                class="fa fa-linkedin l-bg" aria-hidden="true"></i></a>
										
										</h6> 
						</div>
					</div>
					<div class="refer-email has-botom-border">
						<h5 class="text-green py-3">Send as Email Invite</h5>
						<p class="d-flex bordered align-items">
						<input type="email"  class="form-control" id="inviteEmail" aria-describedby="emailHelp"
                                    placeholder="Enter email">
							<!-- <span class="text-light small-text">https:ezgrocery/d/123456</span> -->
						</p>
						<p class="d-flex bordered align-items">
						<input type="email"  class="form-control" id="inviteEmail" aria-describedby="emailHelp"
                                    placeholder="Enter email">
							<!-- <span class="text-light small-text">https:ezgrocery/d/123456</span> -->
						</p>
						<p class="d-flex bordered align-items">
						<input type="email"  class="form-control" id="inviteEmail" aria-describedby="emailHelp"
                                    placeholder="Enter email">
							<!-- <span class="text-light small-text">https:ezgrocery/d/123456</span> -->
						</p>
						<div class="d-flex py-3">
							<h6 class="text-green"><b>Preview Email</b></h6>
							<!-- <span class="ml-auto float-right btn-copy">Send</span> -->
							<div class="form-group col-md-4">
                                <label for="exampleInputEmail1"></label>
                                <a id="invite"
                                    href="mailto:testmail?subject=Invitation to join {{Setting::get('site_title','Ezgrocery')}}&body=Hi,%0A%0A I found this website and thought you might like it. Use my referral code({{\Auth::user()->referral_unique_id}}) on registering in the application.%0A%0AWebsite: {{url('/')}}/login %0AReferral Code: {{\Auth::user()->referral_unique_id}}"
                                    class="btn btn-invite">Send</a>
                            </div>
						</div>
					</div>
					<div class="refer-contact text-center py-4">
						<h6 class="py-2 mb-3"><img src="assets/images/gmail.png" class="img-fluid mail-icon"><b>Share your love of food</b></h6>
						<span class="mx-auto btn-copy">Choose Contacts</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
@section('scripts')
<script type="text/javascript">
    $('#invite').on('click', function(e){
      e.preventDefault();
      var href = $('#invite').attr('href');
      var start = href.indexOf(":");
      var end = href.indexOf("?");
      var email = $('#inviteEmail').val();
      href.substr(start+1, (end-start)-1);
      var url = href.replace(href.substr(start+1, (end-start)-1), email);
      window.location = url;
    });
</script>
@endsection