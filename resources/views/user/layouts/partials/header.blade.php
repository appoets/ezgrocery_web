<header>
        <nav class="navbar navbar-expand-md navbar-dark top-header fixed-top">
            <div class="container">
                <a class="navbar-brand" href="{{url('/')}}"><img src="{{ asset(Setting::get('site_logo', 'logo.png')) }}" class="img-fluid" width="80%"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" >
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                   <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                       <li class="nav-item">
                            <a class="nav-link" href="#"><i class="fa fa-phone"></i>+1-647-977-3250</a>
                        </li>
                        @if(Auth::guest())
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-toggle="modal" data-target="#signup"><i class="fa fa-sign-in"></i>Sign Up</a>
                            </li>
                            <li class="nav-item">
                                <a class="btn btn-green px-4" href="#" data-toggle="modal" data-target="#signin1" style="padding: 3px 20px;">Login</a>
                            </li> 
                            <?php $cart_no =0; ?>
                        @else
                            <?php 
                            $cart = \App\UserCart::list(Auth::user()->id);
                            //dd($cart[0]->product->shop->name);
                            $cart_no = count($cart);?>
                            <li>

                            <li class="nav-item">
                                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user" aria-hidden="true"></i>
                                    <span>{{Auth::user()->name}}</span>
                                </a>
                               <!--  <div class="select_language">
                      <div class="dropdown text-xs-right ml-auto"> -->
                        
                         <div class="dropdown-menu">
                            <ul class="list-unstyled list-inline ct-topbar__list">
                            <ul class="list-unstyled ct-language__dropdown">

                               <!-- <li>
                                    <a href="{{url('/orders')}}">Profile</a>
                                </li> -->
                                <li>
                                    <a class="nav-link" href="{{url('/orders')}}">@lang('user.create.orders')</a>
                                </li>
                                <li>
                                    <a class="nav-link"  href="{{url('/offers')}}">@lang('user.create.offers')</a>
                                </li>
                                <li>
                                    <a class="nav-link" href="{{url('/payments')}}">@lang('user.create.payments')</a>
                                </li>
                                <li>
                                    <a class="nav-link" href="{{url('/favourite')}}">@lang('user.create.fav')</a>
                                </li>
                                <li>
                                    <a class="nav-link" href="{{url('/useraddress')}}">@lang('user.create.address')</a>
                                </li>
                                <li>
                                    <a class="nav-link" href="{{url('/referral')}}">Referral</a>
                                </li>
                                <li>
                                    <a class="nav-link" href="javascript:void(0);" onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    @lang('menu.user.logout')</a>
                                </li>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </ul>
                            </ul>
                            </div><!-- 
                            </div>
                            </div> -->
                        </li>
                        @endif
                        
                        <?php 
                           if($cart_no==0){
                                $url = url('store/details?name=');
                           }else{
                                $url = url('store/details/addcart/'.@$cart[0]->product->shop->name);
                            }
                            ?>
                        @if($cart_no > 0)
                        <li class="nav-item">
                            <a  class="nav-link px-4 btn btn-green"href="{{$url}}"><span class="cart-count">{{$cart_no}}</span>Cart <i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
                        </li>
                       
                        @endif
                            </li>
                        </ul>

                         <ul class="navbar-nav ml-auto">

                        <li class="nav-item pull-right float-right">

                        <a class=" nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><img src="/ca.png">
                            <span>Canada</span>
                        </a>

                        <!-- <div class="dropdown-menu">
                         <ul class="list-unstyled list-inline ct-topbar__list">
                            <ul class="list-unstyled ct-language__dropdown">
                              <li><a href="#" class="nav-link lang-en lang-select" data-lang="en">Canada</a></li>
                            </ul>
                         </ul>
                        </div> -->
                    </li>
                    </ul>


                    <div class="select_language">
                      <div class="dropdown text-xs-right ml-auto">
                        
                        
                        <!-- End of .dropdown-menu -->
                    </div>
                    </div>
                </div>
            </div>
        </nav>
</header>


