  <!--footerr-->
  <section class="footer">
  <footer>
    <div class="container">
      <div class="top-footer">
        <div class="row">
          <div class="col-md-4">
            <h4 class="footer-widget"></h4>
            <a class="navbar-brand" href="{{url('/')}}"><img src="{{ asset(Setting::get('site_footer', 'logo-white.png')) }}" class="img-fluid" width="80%"></a>
            <p class="py-4">Get you groceries pickup or delivered at your convenance.</p>
          </div>
          <div class="col-md-4">
            <h4 class="footer-widget">Quick Links</h4>
            <div class="row">
              <div class="col-md-5">
                <ul class="footer-menu">
                  <li><a href="{{url('/aboutus')}}">About</a></li>
                  <li><a href="{{url('/help')}}">Help</a></li>
                  <li><a href="{{url('/refund')}}">Refund</a></li>
                </ul>
              </div>
              <div class="col-md-7">
                <ul class="footer-menu">
                  <li><a href="{{url('/contact')}}">Contact</a></li>
                  <li><a href="{{url('/terms')}}">Terms & Conditions</a></li>
                  <li><a href="{{url('/privacy')}}">Privacy Policy</a></li>
                  <li><a href="{{url('/otherterms')}}">Press</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <h4 class="footer-widget">Contact Details</h4>
            <div class="contact-details">
              <ul class="footer-menu">
                <li>
                  <i class="fa fa-map-marker"></i><span>123 Road, ABC Street, Edison Avenue,
                  Baltimore, USA</span>
                </li>
                <li>
                   <i class="fa fa-phone"></i><span>+1-647-977-3250</span>
                </li>
                <li>
                   <i class="fa fa-envelope"></i><span>support@ezgrocery.com</span>
                </li>
              </ul>
              <ul class="social-links">
                <li><i class="fa fa-facebook"></i></li>
                <li><i class="fa fa-twitter"></i></li>
                <li><i class="fa fa-linkedin"></i></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="bottom-footer text-center">
        <p>© 2019 - All Rights Reserved,</p>
      </div>
    </div>
  </footer>
</section>

