
		<!-- Nav pills -->
			<ul class="nav nav-pills justify-content-center py-4 mb-5">

				<li  class="nav-item">
					<a class="nav-link @if(Request::segment(1)=='orders') active  @endif" href="{{url('/orders')}}"> Orders</a>
				</li>

				<li  class="nav-item">
					<a class="nav-link @if(Request::segment(1)=='offers') active @endif" href="{{url('/offers')}}">Offers</a>
				</li>

				<li  class="nav-item">
					<a class="nav-link  @if(Request::segment(1)=='favourite') active @endif" href="{{url('/favourite')}}"> Favourites</a>
				</li>
			  
				<li  class="nav-item">
					<a class="nav-link @if(Request::segment(1)=='payments') active @endif" href="{{url('/payments')}}"> Payments</a>
				</li>
				<li  class="nav-item">
					<a class="nav-link @if(Request::segment(1)=='useraddress') active @endif" href="{{url('/useraddress')}}"> Addresses</a>
				</li>

			 
			</ul>
    







<!-- <div class="profile-left col-md-3 col-sm-12 col-xs-12">
    <ul class="nav nav-tabs payment-tabs" role="tablist">
        <li @if(Request::segment(1)=='orders') class="active"  @endif>
            <a href="{{url('/orders')}}"><span><i class="mdi mdi-shopping"></i></span> Orders</a>
        </li>
        <li @if(Request::segment(1)=='offers') class="active"  @endif>
            <a href="{{url('/offers')}}"><span><i class="mdi mdi-percent"></i></span>Offers</a>
        </li>
        <li @if(Request::segment(1)=='favourite') class="active"  @endif>
            <a href="{{url('/favourite')}}"><span><i class="mdi mdi-heart"></i></span> Favourites</a>
        </li>
        <li @if(Request::segment(1)=='payments') class="active"  @endif>
            <a href="{{url('/payments')}}"><span><i class="mdi mdi-credit-card"></i></span> Payments</a>
        </li>
        <li @if(Request::segment(1)=='useraddress') class="active"  @endif>
            <a href="{{url('/useraddress')}}"><span><i class="mdi mdi-map-marker-outline"></i></span> Addresses</a>
        </li>
    </ul>
</div> -->