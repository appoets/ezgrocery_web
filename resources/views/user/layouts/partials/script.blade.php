<script>
    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
</script>



@if(Auth::guest())
<!-- SignIn Modal 1-->
<div class="modal" id="signin1" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-dialog-zoom modal-md" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <!-- Step1-->
                <div class="signin-step1">
                    <h4 class="form-header text-center py-2">Welcome!</h4>
                    <div class="signin-form">
                        <form action="{{ url('/login') }}" id="myform" method="post">
                            {{ csrf_field() }}
                            <div class="form-group row">
                            <div class="col-md-12 first_step_signin">
                            <!-- <div class="input-phone"> -->
                                <!-- <input type="text" id="login_code" name="login_code" class="input-phone form-control" placeholder="+1"> -->

                            <!-- </div> -->

                            <input name="mobile" maxlength="15" class="intl-tel phone phone_no form-control mb-4" id="get_country_code" placeholder="Phone Number"  type="text" >


                            <input type="hidden"  name="country_code" id="country_code_login" value="" >

                            </div>
                            <!-- <div class="col-md-9 p-l-0">

                                <input type="text" class="form-control" id="phone" name="phone"
                                    aria-describedby="emailHelp" placeholder="Phone Number" autocomplete="off">
                            </div> -->
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control pass_finished" name="password" id="password exampleInputPassword1 "
                                    placeholder="Password" autocomplete="off">
                            </div>
                            <p class="text-center btn-login-next1">Forgot your Password? <a href="#">Recover Here</a>
                            </p>
                            <div class="form-group text-center">
                                <button type="submit" disabled="true" class="btn btn-green btn-login mx-auto login_form login_form_sub width-60">Submit</button>
                            </div>
                        </form>

                        <p class="text-center">Don't Have an Account? <a href="#" class="signup_link">Signup Here</a></p>
                    </div>
                </div>

                <!-- step1.1 -->
                <div class="signin-step9 text-center">
                    <h4 class="form-header text-center py-2">Forgot Password</h4>
                    <img src="assets/images/login/key.png" class="py-2 text-center img-fluid" width="15%">
                    <div class="py-2">
                        <h6><b>Please Enter email Address</b></h6>
                    </div>
                    <div class="py-2">
                        <h6 class="err_msg"></h6>
                    </div>
                    <div class="signin-form">

                            <div class="form-group">
                                <input type="email" class="form-control email_recovery" name="email_recovery" id="email_recovery"
                                    aria-describedby="emailHelp" placeholder="Recovery Email Address" autocomplete="off">
                               
                            </div>

                            <div class="form-group text-center">
                                <button type="button"  disabled="true"  class="btn btn-green btn-login mx-auto btn-login-next9 width-60 send-forgot-mail">Next <i
                                        class="fa fa-long-arrow-right"aria-hidden="true"></i></button>
                            </div>
                    </div>
                </div>
                <!-- Step2-->
                <div class="signin-step2 text-center" data-id="10">
                    <h4 class="form-header text-center py-2">Forgot Password</h4>
                    <img src="{{asset('assets/images/login/key.png')}}" class="py-2 text-center img-fluid" width="15%">
                    <div class="err_msg1"></div>
                    <div class="py-2">
                        <h6><b>Please Enter Your Verificatio Code</b></h6>
                        <span>We Have sent an Verification Code for your Registered Email-ID</span>
                    </div>
                    <div class="signin-form">
                        <div class="row">
                            <div class="form-group">
                                <input type="text" class="form-control verification-box" id="otp-verify"
                                    aria-describedby="emailHelp" placeholder="">
                               <!--  <input type="text" class="form-control verification-box" id=""
                                    aria-describedby="emailHelp" placeholder="">
                                <input type="text" class="form-control verification-box" id=""
                                    aria-describedby="emailHelp" placeholder="">
                                <input type="text" class="form-control verification-box" id=""
                                    aria-describedby="emailHelp" placeholder=""> -->
                            </div>
                        </div>

                        <div class="form-group text-center">
                            <button type="button" class="btn btn-green btn-login mx-auto btn-login-next2 width-60">Next <i
                                    class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>

                <!-- Step3-->
                <div class="signin-step3">
                    <h4 class="form-header text-center py-2">Forgot Password</h4>
                    <div class="text-center">
                        <img src="assets/images/login/password.png" class="py-2 text-center img-fluid" width="15%">
                    </div>
                    <div class="text-center py-2">
                        <h6><b>Please Enter a New Password</b></h6>
                    </div>
                    <div class="signin-form">
                            <input type="hidden" name="id" class="user_id">
                        <div class="form-group">
                            <input type="password" class="form-control" id="password1" name="password" aria-describedby="emailHelp"
                                placeholder=" New Password">
                        </div>
                        <div class="form-group">
                            <input type="password" name="password_confirmation" class="form-control" id="password_confirmation1" aria-describedby="emailHelp"
                                placeholder="Re-enter Password">
                        </div>
                        <div class="form-group text-center">
                            <button type="button" class="btn btn-green btn-login mx-auto width-60 change-password">Change Password</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Sign up Modal-->

<!-- SignUp Modal 2-->
<div class="modal" id="signup" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-dialog-zoom modal-md" role="document">
        <div class="modal-content">
            <div class="modal-body">
            <form method="POST" action="{{ url('/register') }}" id="mysignup_form" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="signup-step1">
                    <div class="text-center py-2">
                        <h4 class="form-header text-center py-2">Create Account</h4>
                    </div>
                   
                    <div class="avatar-upload">
                        <div class="avatar-edit">
                            <input type='file' id="imageUpload" name="avatar" accept=".png, .jpg, .jpeg" />
                            <label for="imageUpload"></label>
                        </div>
                        <div class="avatar-preview">
                            <div id="imagePreview" style="background-image: url({{asset('assets/images/avatar.png')}});">
                            </div>
                        </div>
                    </div>
                    <div class="signin-form">
                      
                            <div class="form-group">
                                <input type="text" class="form-control" name="first_name" id="first_name" autocomplete="off"
                                    aria-describedby="emailHelp" 
                                    placeholder=" First Name">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="last_name" name="last_name"
                                    aria-describedby="emailHelp" autocomplete="off" placeholder="Last Name">
                            </div>
                            <div class="form-group text-center">
                                    <button type="button" id="next1" disabled="true"
                                    class="btn btn-green btn-login btn-reg-next1 mx-auto btnSubmit width-60">Next <i
                                        class="fa fa-long-arrow-right"  aria-hidden="true"></i></button>
                            </div>
                    </div>

                     <p class="text-center">Already't Have an Account? <a href="#" class="signin_link">Signin Here</a></p>
                </div>

                <!---step 2-->
                <div class="signup-step2">
                    <div class="text-center py-2">
                        <h4 class="form-header text-center py-2">Your Mail ID</h4>
                    </div>
                    <div class="text-center">
                        <img src="{{asset('assets/images/login/mail.png')}}" class="py-2 text-center img-fluid" width="15%">
                    </div>
                    <div class="signin-form">
                        <div class="form-group">
                            <input type="email" class="form-control" id="email" name="email" autocomplete="off"
                                aria-describedby="emailHelp" placeholder=" Email" required>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" id="password_signup" name="password" autocomplete="off"pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 6 or more characters"
                                aria-describedby="emailHelp" placeholder="Password" required>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control password_confirmation_signup" id="password_confirmation " autocomplete="off"
                                name="password_confirmation" aria-describedby="emailHelp" placeholder="Confirm Password"
                                required>
                        </div>
                        <div class="form-group text-center">
                            <button type="button" id="back1"
                                    class="btn btn-green btn-login btn-reg-back1 mx-auto width-30"><i
                                        class="fa fa-long-arrow-left" aria-hidden="true"></i>&nbsp;Back </button>
                            <button type="button" disabled="true" class="btn btn-green btn-login btn-reg-next2 mx-auto width-30">Next <i
                                    class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>

                <!---step 3-->
                <div class="signup-step3">
                    <div class="text-center py-2">
                        <h4 class="form-header text-center py-2">Your Phone Number</h4>
                    </div>
                    <div class="text-center">
                        <img src="{{asset('assets/images/login/phone.png')}}" class="py-2 text-center img-fluid" width="15%">
                    </div>
                    <div class="signin-form">
                    <div class="print-error-msg">
                            <ul class="list-unstyled"></ul>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12" id="signup_country_code">

                                <input name="phone_number" maxlength="15" class="intl-tel phone  form-control mb-4" id="signup_mobile" placeholder="Phone Number"  type="text" >

                            </div>
                            <!-- <div class="col-md-3">
                                <input type="text" id="country_code" name="country_code" class="country_code form-control" placeholder="+1">
                            </div>
                            <div class="col-md-9 p-l-0">
                                <input type="number" min="0" class="form-control phone-number" id="phone_number" name="phone_number" autocomplete="off" placeholder="@lang('user.enter_phone')" maxlength="10" onkeypress="return isNumberKey(event);" required>
                            </div>  -->
                                                
                        </div>


                        <div class="form-group text-center">
                             <button type="button" id="back2" 
                                    class="btn btn-green btn-login btn-reg-back2 mx-auto width-30"><i
                                        class="fa fa-long-arrow-left" aria-hidden="true"></i>&nbsp;Back </button>
                            <button type="button" onclick="smsLogin();" disabled="true"
                                class="btn btn-green btn-login btn-reg-next3 mx-auto width-30">Next <i
                                    class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
                <!-- Step4-->
                <div class="signup-step4 text-center">
                <div class="otp_header">
                    <h4 class="form-header text-center py-2">Verify Account</h4>
                    <img src="{{asset('assets/images/login/key.png')}}" class="py-2 text-center img-fluid" width="15%">
                    <div class="py-2" id="show_otp_msg">
                        <span id="sent_otp_no">Enter OTP sent to Mobile Number</span>
                    </div>
                    </div>
                    <div class="referral_header" style="display:none">
                    <h4 class="form-header text-center py-2">Referral Code(Optional)</h4>
                    <img src="{{asset('assets/images/login/key.png')}}" class="py-2 text-center img-fluid" width="15%">
                    
                    </div>
                    <div class="signin-form">
                  <div class="otp_show">
                            <div class="form-group">
                                <input type="text" class="form-control otp_check verification-box"  name="otp" id="otp" autocomplete="off" maxlength="6" >

                                <!-- <input type="text" class="form-control verification-box" id="otp" name=otp maxlength="6"
                                    aria-describedby="emailHelp" placeholder="">
                                <input type="text" class="form-control verification-box" id="otp" name=otp maxlength="6"
                                    aria-describedby="emailHelp" placeholder="">
                                <input type="text" class="form-control verification-box" id="otp" name=otp maxlength="6"
                                    aria-describedby="emailHelp" placeholder="">
                                <input type="text" class="form-control verification-box" id="otp" name=otp maxlength="6"
                                    aria-describedby="emailHelp" placeholder=""> -->

                            </div>

                        <input type="hidden" id="otp_ref" name="otp_ref" value="" />
                        <input type="hidden" id="otp_phone" name="phone" value="" />

                        <div class="form-group text-center">
                        <button type="button" id="back3"  class="btn btn-green btn-login btn-reg-back3 mx-auto"><i class="fa fa-long-arrow-left" aria-hidden="true"></i>&nbsp;Back </button>

                            <button type="button" class="btn btn-green btn-login mx-auto  mobile_otp_verfication" disabled="true" id="verify_btn"
                                onclick="checkotp();" value="Verify Otp" style="display: none;">Verify<i
                                    class="fa fa-long-arrow-right" aria-hidden="true"></i></button>

                            <!-- <button type="submit" onclick="checkotp();" class="btn btn-green btn-login mx-auto ">Verify <i class="fa fa-long-arrow-right" aria-hidden="true"></i></button> -->
                        </div>
                        <!-- <p class="py-2 text-center"> -->
                            <a href="#" class="resend_otp" onclick="smsLogin();">Didn't received otp? </a>
                        <!-- </p> -->
</div>
                        @if(Setting::get('referral') == 1)
                            <div class="col-md-12" id="referral" style="display:none">
                                <input type="text" placeholder="Referral Code (Optional)" class="form-control" name="referral_code" >

                                @if ($errors->has('referral_code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('referral_code') }}</strong>
                                    </span>
                                @endif
                            </div>
                            @else
                                <input type="hidden" name="referral_code" >
                            @endif
                        <div class="form-group text-center">
                            <!-- <button type="button" class="login-btn register_btn">@lang('user.footer.sign_up')</button> -->
                            <button type="submit" class="btn btn-green btn-login mx-auto final_submit width-60" style="display:none" disabled="true">Submit <i
                                    class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
                </form>

            </div>
        </div>
    </div>
</div>
<!-- End Sign up Modal-->

@endif
<style>
	.pac-container {
		z-index: 9999999999999999999 !important;
	}
</style>


 <script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.js"></script>
<script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/additional-methods.js"></script>

<style>
    .intl-tel-input {
    width: 100% !important;
    margin-bottom: 1.5rem !important;
} 
  

.intl-tel-input input {
    padding: 15px 45px !important;
    border-radius: 4px !important;
        padding-left: 17% !important;
}


.intl-tel-input .selected-flag {
      width: 55px !important;
     height: 55px !important; 
    background: #d9d9d9;
}
.intl-tel-input.allow-dropdown .selected-flag:focus {
    outline: 0 !important
}
</style>
<script type="text/javascript">
 $(".pass_finished, .phone_no").keyup(function () {
if ($('.pass_finished').val().trim() != "" && $(".phone_no").val() != "" && $(".phone_no").val().length == 10) {
    $('.login_form_sub').prop('disabled',false);
} 

});

  $(".password_confirmation_signup").keyup(function () {
if ($(this).val().trim() != "") {
    $('.btn-reg-next2').prop('disabled',false);
} 

});


$(".email_recovery").keyup(function () {
if ($(this).val().trim() != "") {
    console.log('tst1');
    $('.btn-login-next9').prop('disabled',false);
} 

});
if($(".email_recovery").val() != ''){
     $('.btn-login-next9').prop('disabled',false);
}
$(".otp_check").keyup(function () {
if ($('.otp_check').val().trim() != "" && $(".otp_check").val().length == 6) {
    $('#verify_btn').prop('disabled',false);
} 

});

$("#signup_mobile").keyup(function () {
if ($(this).val().trim() != "") {
    $('.btn-reg-next3').prop('disabled',false);
} 

});
</script>
<script type="text/javascript">

   $(".phone").intlTelInput({
      initialCountry: "ca",
    });

</script>
<script type="text/javascript">
 $(document).ready(function() {

    $('#get_country_code').change(function(){

  
        var countryCode_signin= $('.first_step_signin .phone').intlTelInput('getSelectedCountryData').dialCode;

        var prefix = "+";

        $('#country_code_login').val(prefix + '' + countryCode_signin);

    });

});
</script>

<script type="text/javascript">
    $(function () {
        $("#last_name").keyup(function () {

            if ($(this).val().trim() != "") {
                $('.btn-reg-next1').prop('disabled',false);
            } 
            
        });

    });
  


</script>



<script type="text/javascript">
		$(document).ready(function(){

			
             $(".signup-step2, .signup-step3, .signup-step4").hide();

			$(".btn-reg-next1").click(function(){
				var sigForm = $("#mysignup_form");
				sigForm.validate({
					errorElement: 'span',
					errorClass: 'help-block',
					highlight: function(element, errorClass, validClass) {
						$(element).closest('.form-group').addClass("has-error");
					},
					unhighlight: function(element, errorClass, validClass) {
						$(element).closest('.form-group').removeClass("has-error");
					},
					rules: {
						first_name: {
							required: true,
							
						},
                        last_name: {
							required: true,
						
						},
                      
                        password : {
							required: true,
						},
						password_confirmation : {
							required: true,
							 equalTo: '#password_signup',
						},
					
						email:{
							required: true,
						},
                        phone_number:{
                            required: true,

                        },
                       
						
					},
					messages: {
						first_name: {
							required: "First Name required",
						},
					
						last_name: {
							required: "Last Name required",
						},
                        password : {
							required: "Password required",
						},
						password_confirmation : {
							required: "Password required",
							equalTo: "Password don't match",
						},
					
						email: {
							required: "Email required",
						},
                        phone_number:{
                            required: "Mobile Number required",

                        },
                   
					}
				});
				if (sigForm.valid() === true){

                    $(".signup-step2").show();
                    $(".signup-step1, .signup-step3, .signup-step4").hide();
				}
             
			});


            $(".btn-reg-next2").click(function () {
				var sigForm = $("#mysignup_form");
				sigForm.validate({
					errorElement: 'span',
					errorClass: 'help-block',
					highlight: function(element, errorClass, validClass) {
						$(element).closest('.form-group').addClass("has-error");
					},
					unhighlight: function(element, errorClass, validClass) {
						$(element).closest('.form-group').removeClass("has-error");
					},
					rules: {
					
                      
                        password : {
							required: true,
						},
						password_confirmation : {
							required: true,
							 equalTo: '#password_signup',
						},
					
						email:{
							required: true,
						},
                        
                      
						
					},
					messages: {
						
                        phone_number:{
                            required: "Mobile Number required",

                        },
                        password : {
							required: "Password required",
						},
						password_confirmation : {
							required: "Password required",
							equalTo: "Password don't match",
						},
					
						email: {
							required: "Email required",
						},
                       
                      
                   
					
					}
				});
				if (sigForm.valid() === true){

                    $(".signup-step3").show();
                        $(".signup-step1, .signup-step2, .signup-step4").hide();
				}
             
			});
           
            //  $(".btn-reg-next3").click(function () {
			// 	var sigForm = $("#mysignup_form");
			// 	sigForm.validate({
			// 		errorElement: 'span',
			// 		errorClass: 'help-block',
			// 		highlight: function(element, errorClass, validClass) {
			// 			$(element).closest('.form-group').addClass("has-error");
			// 		},
			// 		unhighlight: function(element, errorClass, validClass) {
			// 			$(element).closest('.form-group').removeClass("has-error");
			// 		},
			// 		rules: {
            //             phone_number:{
            //                     required: true,

            //                 }
                      
            //             // otp:{
            //             //     required: true,

            //             // }
					
						
			// 		},
			// 		messages: {
						
            //             // otp:{
            //             //     required: "OTP required",

            //             // },
            //              phone_number:{
            //                 required: "Mobile Number required",

            //             },
                       
					
			// 		}
			// 	});
			// 	if (sigForm.valid() === true){
            //         $(".signup-step4").show();
            //         $(".signup-step1, .signup-step2, .signup-step3").hide();
			// 	}
             
			// });

        });
</script>


<script type="text/javascript">

$(document).ready(function () {

    // $('body').on('hidden.bs.modal', function () {
    //     $(this).find('form').trigger('reset');
    // })

    $('body').on('hidden.bs.modal', function() {
        document.location.reload();

    });

});
$(document).ready(function () {
    $(".signin-step2, .signin-step3, .signin-step9").hide();

    $(".login_form").click(function(){

        var form = $("#myform");
                    form.validate({
                        errorElement: 'span',
                        errorClass: 'help-block',
                        highlight: function(element, errorClass, validClass) {
                            $(element).closest('.form-group').addClass("has-error");
                        },
                        unhighlight: function(element, errorClass, validClass) {
                            $(element).closest('.form-group').removeClass("has-error");
                        },
                        rules: {
                            password: {
                                required: true,
                            },
                            mobile: {
                                required: true,
                            },
                           
                        
                            
                        },
                        messages: {
                            mobile: {
                                required: "Mobile Number required",
                            },
                        
                            password: {
                                required: "Password required",
                            },
                          
                        
                        }
                    });
                    if (form.valid() === true){
                        // $('.login_form').prop('disabled',false);
                    //   $('.login_form').prop('disabled',true);


                    }
                    
    });
   

});

$(document).ready(function () {

$(".change-password").click(function(){
console.log('test');
    var id = $('.user_id').val();
    var password = $('#password1').val();
    var password_confirmation = $('#password_confirmation1').val();
    $.ajax({
            type:"POST",
            url:"{{url('/password/reset')}}",
            data:{'id':id,'password':password,'password_confirmation':password_confirmation,'_token': '{{ csrf_token() }}'},
            success : function(results) {
                console.log(results);
                alert('password updated');
                    location.reload();
                
                return false;
            }
        });
    
});

$(".btn-login-next9").click(function(){
console.log('test');

    var email = $('#email_recovery').val();
    console.log(email);
    $.ajax({
            type:"POST",
            url:"{{url('/password')}}",
            data:{'email':email,'_token': '{{ csrf_token() }}'},
            success : function(results) {
                console.log(results.message);
                if(results.message == "User Not Exist"){
                    $('.err_msg').text(results.message);
                }else{
                    $('.user_id').val(results.user.id);
                    $(".signin-step2").show();
                    $(".signin-step1, .signin-step3, .signin-step9").hide();
                }
                return false;
            }
        });

    
});
});
</script>


 <!-- <style type="text/css">
        .easy-autocomplete-container ul { max-height: 200px !important; overflow: auto !important; }
        .easy-autocomplete { width:90px!important; }
        .phone_fileds {
            margin-left: 0px !important;
            border-left: 1px solid #ccc !important;
            width: 100% !important
        }
        .no-pad{
            padding: 0px !important;
        }
    </style> -->
 <script type="text/javascript">
function phoneNumberKey(evt)
{
    console.log('hi');
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31 
    && (charCode < 48 || charCode > 57))
        return false;

    return true;
}    
function isNumberKey(evt)
{
    var edValue = document.getElementById("phone_number");
    var s = edValue.value;
    if (event.keyCode == 13) {
        event.preventDefault();
        if(s.length>=10){
            smsLogin();
        }
    }
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31 
    && (charCode < 48 || charCode > 57))
        return false;

    return true;
}    
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
// var options = {

//   url: "{{asset('assets/user/js/countryCodes.json')}}",

//   getValue: "dial_code",

//   list: {
//     match: {
//       enabled: true
//     },
//     onClickEvent: function() {
//             var value = $(".country_code").getSelectedItemData().dial_code;

//             $(this).val(value).trigger("change");
//         },
//     maxNumberOfElements: 1000
//   },
//   minCharNumber: 1,
//   template: {
//     type: "custom",
//     method: function(value, item) {
//       return "<span class='flag flag-" + (item.dial_code).toLowerCase() + "' ></span>" +value+" ( "+item.name+" ) ";
//     }
//   },

//   theme: "round"
// };
// $(".country_code").easyAutocomplete(options);
</script>
 
<script type="text/javascript">
    function googleTranslateElementInit() {
        new google.translate.TranslateElement({
            pageLanguage: 'en',
            layout: google.translate.TranslateElement.FloatPosition.TOP_LEFT
        }, 'google_translate_element');
    }

    function triggerHtmlEvent(element, eventName) {
        var event;
        if (document.createEvent) {
            event = document.createEvent('HTMLEvents');
            event.initEvent(eventName, true, true);
            element.dispatchEvent(event);
        } else {
            event = document.createEventObject();
            event.eventType = eventName;
            element.fireEvent('on' + event.eventType, event);
        }
    }

    jQuery('.lang-select').click(function () {
        var theLang = jQuery(this).attr('data-lang');
        jQuery('.goog-te-combo').val(theLang);

        //alert(jQuery(this).attr('href'));
        window.location = jQuery(this).attr('href');
        location.reload();

    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#testimonial-slider").owlCarousel({
            items: 1,
            itemsDesktop: [1000, 1],
            itemsDesktopSmall: [979, 1],
            itemsTablet: [768, 1],
            pagination: false,
            navigation: true,
            navigationText: ["", ""],
            transitionStyle: "goDown",
            autoPlay: false
        });

        $(".fav-slider").owlCarousel({
            items: 4,
            itemsDesktop: [1000, 1],
            itemsDesktopSmall: [979, 1],
            itemsTablet: [768, 1],
            pagination: false,
            navigation: true,
            navigationText: ["", ""],
            transitionStyle: "goDown",
            autoPlay: false
        });



    });
</script>

<script type="text/javascript">
    function checkotp() {

        var otp = document.getElementById("otp").value;
        var my_otp = $('#otp_ref').val();
        if (otp) {
            if (my_otp == otp) {
                $('.referral_header').show();
                $('.resend_otp').hide();
                $('#referral').show();
                $('.sent_otp_no').hide();
                $('.otp_show').hide();
                $('.otp_header').hide();
                $('.final_submit').show();
                $('#back3').hide();
                $('.final_submit').prop('disabled',false);
                $(".print-error-msg").find("ul").html('');
                $('#mobile_otp_verfication').html("<p class='helper'> Please Wait... </p>");
                $('#phone_number').attr('readonly', true);
                $('#country_code').attr('readonly', true);
                $('.mobile_otp_verfication').hide();
                $('#second_step').fadeIn(400);
                $('#mobile_verfication').show().html("<p class='helper'> * Phone Number Verified </p>");
                my_otp = '';
            } else {
                $(".print-error-msg").find("ul").html('');
                $(".print-error-msg").find("ul").append('<li>Otp not Matched!</li>');
            }
        }
    }



    function smsLogin() {

        // $('.exist-msg').hide();
        var countryCode = $('#signup_country_code .phone').intlTelInput('getSelectedCountryData').dialCode;
        var phoneNumber = $('#signup_country_code input[name=phone_number]').val();

        var prefix_code = "+";
       
       var send_otp_text = "Enter OTP sent to ";

        $('#otp_phone').val(prefix_code+''+countryCode + '' + phoneNumber);
        $("#sent_otp_no").html(send_otp_text+''+prefix_code+''+countryCode+''+phoneNumber);

        var csrf = $("input[name='_token']").val();;
        var  phone = prefix_code+''+countryCode+''+phoneNumber;

        $.ajax({
            url: "{{url('/otp')}}",
            type: 'POST',
            data: {
                phone: phone,
                '_token': csrf,
                phoneonly: phoneNumber
            },

            success: function (data) {
                if ($.isEmptyObject(data.error)) {
                    $(".signup-step4").show();
                    $(".signup-step1, .signup-step2, .signup-step3").hide();
                    $('#otp_ref').val(data.otp);
                    $('.mobile_otp_verfication').show();

                    $('#mobile_verfication').hide();
                    $('#mobile_verfication').html("<p class='helper'> Please Wait... </p>");
                    $('#phone_number').attr('readonly', true);
                    $('#country_code').attr('readonly', true);
                    $(".print-error-msg").find("ul").html('');
                    $(".print-error-msg").find("ul").append('<li>' + data.message + '</li>');
                } else {

                    printErrorMsg(data.error);
                }
            },
            error: function (jqXhr, status) {
                if (jqXhr.status === 422) {
                    $(".print-error-msg").show();
                    var errors = jqXhr.responseJSON;
                    $.each(errors, function (key, value) {
                        console.log(value);
                        $(".print-error-msg").find("ul").append('<li class="alert-warning">' + value + '</li>');
                    });
                }
            }

        });
    }

    function printErrorMsg(msg) {

        $(".print-error-msg").find("ul").html('');
        $(".print-error-msg").css('display', 'block');

        $(".print-error-msg").show();

        $(".print-error-msg").find("ul").append('<li><p>' + msg + '</p></li>');

    }




    $(document).ready(function () {
        $('.customer-logos').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 1500,
            arrows: false,
            dots: false,
            pauseOnHover: false,
            responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            }, {
                breakpoint: 520,
                settings: {
                    slidesToShow: 2
                }
            }]
        });
    });
</script>

<script type="text/javascript">
    //login page
    $(document).ready(function () {
        $(".signin-step2, .signin-step3, .signin-step9").hide();
        // $(".btn-login-next9").click(function () {
        //     event.preventDefault()
        //     $(".signin-step2").show();
        //     $(".signin-step1, .signin-step3, .signin-step9").hide();
        // });
        $(".btn-login-next1").click(function () {
            event.preventDefault()
           $(".signin-step9").show();
           $(".signin-step1, .signin-step3, .signin-step2").hide();
           
        });
        $(".btn-login-next2").click(function () {
            event.preventDefault()
               var email = $('#email_recovery').val();
             var otp = $('#otp-verify').val();
            console.log(otp);
            $.ajax({
                    type:"POST",
                    url:"{{url('/checkotpmail')}}",
                    data:{'email':email,'otp':otp,'_token': '{{ csrf_token() }}'},
                    success : function(results) {
                        console.log(results.message);
                        if(results.message == "Otp wrong"){
                            $('.err_msg1').text(results.message);
                        }else{
                           $(".signin-step3").show();
                           $(".signin-step1, .signin-step2, .signin-step9").hide(); 
                        }
                        return false;
                    }
                });
            
        });
    });
        
    $(document).ready(function () {
        // $("p a.signup_link").click(function () {
        // alert("yes");
        // $(".signup-step1").show();
        // });
         $('body').on('click', 'a.signup_link', function() {
            $("#signin1").hide()
            $("#signup").show();
        });
    });

    $(document).ready(function () {
        // $("p a.signup_link").click(function () {
        // alert("yes");
        // $(".signup-step1").show();
        // });
         $('body').on('click', 'a.signin_link', function() {
            $("#signin1").show()
            $("#signup").hide();
        });
    });


    //register page
    $(document).ready(function () {
        $(".signup-step2, .signup-step3, .signup-step4").hide();
        // $(".btn-reg-next1").click(function () {
        //     event.preventDefault()
        //     $(".signup-step2").show();
        //     $(".signup-step1, .signup-step3, .signup-step4").hide();
        // });
        // $(".btn-reg-next2").click(function () {
        //     event.preventDefault()
        //     $(".signup-step3").show();
        //     $(".signup-step1, .signup-step2, .signup-step4").hide();
        // });
        // $(".btn-reg-next3").click(function () {
        //     event.preventDefault()
        //     $(".signup-step4").show();
        //     $(".signup-step1, .signup-step2, .signup-step3").hide();
        // });
    });

    $(document).ready(function () {
          $("#back1").click(function () {
            event.preventDefault()
            $(".signup-step1").show();
            $(".signup-step2, .signup-step3, .signup-step4").hide();
        });
        $("#back2").click(function () {
            event.preventDefault()
            $(".signup-step2").show();
            $(".signup-step1, .signup-step3, .signup-step4").hide();
        });
        $("#back3").click(function () {
            $('.print-error-msg ul').html('');
            event.preventDefault()
            $(".signup-step3").show();
            $(".signup-step1, .signup-step2, .signup-step4").hide();
        });
    });
</script>

<!-- image Upload -->
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
                $('#imagePreview').hide();
                $('#imagePreview').fadeIn(650);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#imageUpload").change(function () {
        readURL(this);
    });
</script>

<!-- add card page -->
<script type="text/javascript">
    $(document).ready(function () {
        $(".card-block-2").hide();
        $(".card-next-1").click(function () {
            event.preventDefault()
            $(".card-block-2").show();
            $(".card-block-1").hide();
        });
    });
</script>

<!-- add address page -->
<script type="text/javascript">
    $(document).ready(function () {
        $(".address-form").hide();
        $(".add-address").click(function () {
            event.preventDefault()
            $(".address-form").show();
            $(".address-box, .add-address-row").hide();
        });
    });
</script>

<!-- View Orders page -->
<!-- <script type="text/javascript">
    $(document).ready(function () {
        $(".order-details-box").hide();
        $(".view-details").click(function () {
            event.preventDefault()
            $(".order-details-box").show();
            $(".store-list").hide();
        });
    });
</script> -->

<script type="text/javascript">
    function increaseValue() {
        var value = parseInt(document.getElementById('number').value, 10);
        value = isNaN(value) ? 0 : value;
        value++;
        document.getElementById('number').value = value;
    }

    function decreaseValue() {
        var value = parseInt(document.getElementById('number').value, 10);
        value = isNaN(value) ? 0 : value;
        value < 1 ? value = 1 : '';
        value--;
        document.getElementById('number').value = value;
    }

    function increaseValue1() {
        var value = parseInt(document.getElementById('number1').value, 10);
        value = isNaN(value) ? 0 : value;
        value++;
        document.getElementById('number1').value = value;
    }

    function decreaseValue1() {
        var value = parseInt(document.getElementById('number1').value, 10);
        value = isNaN(value) ? 0 : value;
        value < 1 ? value = 1 : '';
        value--;
        document.getElementById('number1').value = value;
    }
</script>

<!-- 
@if(Request::segment(1)=='restaurant' && Request::has('myaddress'))
<div class="modal" id="add_address" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-dialog-zoom modal-md" role="document">
        <div class="modal-content">
            <div class="modal-body">

                <div class="row address-form">
                    <div class="col-md-6">
                        <form action="{{route('useraddress.store')}}" method="POST" id="comon-form" class="common-form">
                            {{ csrf_field() }}
                            <div class="input-section">
                                <div class="form-group">
                                    <label>Address</label>
                                    <input class="form-control addr-mapaddrs" id="pac-input" name="map_address"
                                        type="text" value="{{Session::get('search_loc')}}"
                                        placeholder="enter your address">
                                    <input type="hidden" id="latitude" name="latitude"
                                        value="{{ Session::get('latitude') }}" readonly required>
                                    <input type="hidden" id="longitude" name="longitude"
                                        value="{{ Session::get('longitude') }}" readonly required>
                                </div>
                                <div class="form-group">
                                    <label>Door / Flat no.</label>
                                    <input class="form-control addr-building" name="building" type="text" value="">
                                </div>
                                <div class="form-group">
                                    <label>Landmark</label>
                                    <input class="form-control addr-landmark" name="landmark" type="text">
                                </div>
                                <div class="form-group">
                                    <label>Address Type</label>
                                    <select class="form-control addr-type" name="type">
                                        @foreach($add_type as $key => $item)
                                        <option value="{{$key}}">{{$item}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <button class="btn btn-green float-right">Save &amp; Proceed</button>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <div class="" id="my_map" style="width: 100%; height: 200px;"></div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif --->


@if(Request::segment(1)=='restaurant' && Request::has('myaddress'))
 <div class="modal" id="add_address" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-dialog-zoom modal-md" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <!-- Step1-->
                <div class="signin-step1">
                    <h4 class="form-header text-center py-2">Add Address</h4>
                    <form action="{{route('useraddress.store')}}" method="POST" id="comon-form" class="common-form">
                            {{ csrf_field() }}     
                            <div class="" id="my_map" style="width: 100%; height: 200px;"></div>
                   
                        <div class="form-group">
                        <label>Address</label>
                                    <input class="form-control addr-mapaddrs" id="pac-input" name="map_address"
                                        type="text" value="{{Session::get('search_loc')}}"
                                        placeholder="enter your address">
                                    <input type="hidden" id="latitude" name="latitude"
                                        value="{{ Session::get('latitude') }}" readonly required>
                                    <input type="hidden" id="longitude" name="longitude"
                                        value="{{ Session::get('longitude') }}" readonly required>
                            <!-- <input type="text" class="form-control" placeholder="Address"> -->
                        </div>
                        <div class="form-group">
                        <label>Door / Flat no.</label>
                                    <input class="form-control addr-building" name="building" type="text" value="">
                            <!-- <input type="text" class="form-control" placeholder="Zip Code"> -->
                        </div>
                        <div class="form-group">
                            <!-- <input type="text" class="form-control" placeholder="Landmark"> -->
                            <label>Landmark</label>
                                    <input class="form-control addr-landmark" name="landmark" type="text">
                        </div>
                        <div class="form-group">
                            <!-- <select class="form-control" id="" placeholder="Add Address Type">
                                <option selected>Home</option>
                                <option>Work</option>
                            </select> -->
                            <label>Address Type</label>
                                    <select class="form-control addr-type" name="type">
                                        @foreach($add_type as $key => $item)
                                        <option value="{{$key}}">{{$item}}</option>
                                        @endforeach
                                    </select>
                        </div>
                        <!-- <a class="btn btn-green float-right" href="#">Save Address </a> -->
                        <button class="btn btn-green float-right">Save &amp; Proceed</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

@if(Request::segment(1)=='' || Request::segment(1)=='stores' || Request::segment(1)=='useraddress' ||
Request::segment(1)=='orders' || Request::get('myaddress') || Request::segment(1)=='restaurant')
<script>
    var map;
    var input = document.getElementById('pac-input');
    var latitude = document.getElementById('latitude');
    var longitude = document.getElementById('longitude');
    var input_cur = document.getElementById('pac-input_cur');
    var latitude_cur = document.getElementById('latitude_cur');
    var longitude_cur = document.getElementById('longitude_cur');
    var address = document.getElementById('address');

    function initMap() {

        var userLocation = new google.maps.LatLng(
            13.066239,
            80.274816
        );

        map = new google.maps.Map(document.getElementById('my_map'), {
            center: userLocation,
            zoom: 15
        });

        var service = new google.maps.places.PlacesService(map);
        var autocomplete = new google.maps.places.Autocomplete(input);
        var infowindow = new google.maps.InfoWindow();

        autocomplete.bindTo('bounds', map);
        autocomplete.setFields(['geocode']);

        var infowindow = new google.maps.InfoWindow({
            content: "Location",
        });

        var marker = new google.maps.Marker({
            map: map,
            draggable: true,
            anchorPoint: new google.maps.Point(0, -29)
        });

        marker.setVisible(true);
        marker.setPosition(userLocation);
        infowindow.open(map, marker);

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (location) {
                console.log(location);
                var userLocation = new google.maps.LatLng(
                    location.coords.latitude,
                    location.coords.longitude
                );

                latitude_cur.value = location.coords.latitude;
                longitude_cur.value = location.coords.longitude;


                //var latLngvar = location.coords.latitude+' '+location.coords.longitude+"   ";
                var latlng = {
                    lat: parseFloat(location.coords.latitude),
                    lng: parseFloat(location.coords.longitude)
                };
                getcustomaddress(latlng);
                marker.setPosition(userLocation);
                map.setCenter(userLocation);
                map.setZoom(13);
            });
        } else {
            // Browser doesn't support Geolocation
            handleLocationError(false, infoWindow, map.getCenter());
        }

        google.maps.event.addListener(map, 'click', updateMarker);
        google.maps.event.addListener(marker, 'dragend', updateMarker);

        function getcustomaddress(latLngvar) {
            var geocoder = new google.maps.Geocoder();
            console.log(latLngvar);
            geocoder.geocode({
                'latLng': latLngvar
            }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    //console.log(results[0]);
                    if (results[0]) {

                        input_cur.value = results[0].formatted_address;

                        //updateForm(event.latLng.lat(), event.latLng.lng(), results[0].formatted_address);
                    } else {
                        alert('No Address Found');
                    }
                } else {
                    alert('Geocoder failed due to: ' + status);
                }
            });
        }

        function updateMarker(event) {
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({
                'latLng': event.latLng
            }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        input.value = results[0].formatted_address;
                        updateForm(event.latLng.lat(), event.latLng.lng(), results[0].formatted_address);
                    } else {
                        alert('No Address Found');
                    }
                } else {
                    alert('Geocoder failed due to: ' + status);
                }
            });

            marker.setPosition(event.latLng);
            map.setCenter(event.latLng);
        }

        autocomplete.addListener('place_changed', function (event) {
            marker.setVisible(false);
            var place = autocomplete.getPlace();

            if (place.hasOwnProperty('place_id')) {
                if (!place.geometry) {
                    window.alert("Autocomplete's returned place contains no geometry");
                    return;
                }
                updateLocation(place.geometry.location);
            } else {
                service.textSearch({
                    query: place.name
                }, function (results, status) {
                    if (status == google.maps.places.PlacesServiceStatus.OK) {
                        updateLocation(results[0].geometry.location, results[0].formatted_address);
                        input.value = results[0].formatted_address;

                    }
                });
            }
        });

        function updateLocation(location) {
            map.setCenter(location);
            marker.setPosition(location);
            marker.setVisible(true);
            infowindow.open(map, marker);
            updateForm(location.lat(), location.lng(), input.value);
        }

        function updateForm(lat, lng, addr) {
            console.log(lat, lng, addr);
            latitude.value = lat;
            longitude.value = lng;
            @if(Request::get('search_loc'))
            $('#my_map_form').submit();
            @endif
        }
    }
    $('.my_map_form_current').on('click', function () {
        $('#my_map_form_current').submit();
    })

    /*$('.pac-input').on('blur',function(){
        if($('#latitude').val()!=''){
            $('#my_map_form').submit();
        }
    })*/
</script>



@endif
<script
    src="https://maps.googleapis.com/maps/api/js?key={{Setting::get('GOOGLE_API_KEY')}}&libraries=places&callback=initMap"
    async defer></script>