hello!,<br/>
You are receiving this email because we received a password reset request for your account.<br/>
Your OTP is <b>{{$order->otp}}</b>
<br/>
If you did not request a password reset, no further action is required.<br/>
Regards,<br/><br/>
Admin<br/>
ezgrocery.ca<br/>