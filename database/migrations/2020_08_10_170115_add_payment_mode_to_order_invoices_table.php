<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaymentModeToOrderInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_invoices', function (Blueprint $table) {
            DB::statement("ALTER TABLE order_invoices CHANGE COLUMN payment_mode payment_mode ENUM( 'cash',
            'stripe',
            'paypal',
            'braintree',
            'wallet',
            'ripple',
            'eather',
            'bitcoin','elavon') NOT NULL DEFAULT 'cash'");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_invoices', function (Blueprint $table) {
            //
        });
    }
}
